# Internal Project Monthly Testing 

## ISSUES FOUND
**NOTE:** Any issues found need to be noted in a new ticket in the backlog.

**FILED ISSUE TICKET NUMBERS:**

## UI/UX
### Responsiveness
Use real devices or browserstack
- [ ] Chrome
- [ ] Safari
- [ ] Firefox
- [ ] Edge

### Color and Appearance
- [ ] Hyperlink colors are standard and consistent
- [ ] ADA compliance (run Lighthouse for full audit)
- [ ] Consistent link colors
- [ ] Consistent button format, size, and color
- [ ] Consistent font style and size (typography hierarchy)
- [ ] No existence of orphans (single word of a title or header that hangs on a new line by itself)

### Images
- [ ] Check image alignments
- [ ] Size optimization (can be gathered using Lighthouse)
- [ ] Render speed optimization
- [ ] Text wraps properly around images

### Navigation
- [ ] Nav links work properly
- [ ] Scroll bar appears when necessary
- [ ] Link to homepage available on every page

### Other
- [ ] Readable font
- [ ] Spell-checked
- [ ] Page urls should be directional/explicit
   - [ ] Favicon exists

 ## CONTENT
While we are not 100% responsible for content, we should point out where content isn’t consistent.
- [ ] Spot-check content for consistency
- [ ] Perform a Google search and see what the result look like (some SEO scans can help with content)

## FUNCTIONALITY
- [ ] Back button
- [ ] Links render properly
- Forms 
- [ ] Render properly
- [ ] Submit properly
- [ ] Default values populate fields
- [ ] Inline form field validation (“Please specify valid email.”)
- [ ] Input values populate the database
- [ ] Mandatory fields 
   - [ ] Do not submit when blank
   - [ ] Explicit error message displays
- Inputs
- [ ] Explicit instruction
- [ ] Submits with keyboard “enter” or app button 

## SECURITY
- [ ] Confirm SSL/TLS certification via HTTPS
- [ ] Check that passwords are displayed in an encrypted format
- [ ] Check for SQL Injection for all pages that accept user-entered input to access the database
- [ ] Confirm that security error messages do not contain sensitive information
- [ ] Check that secure pages cannot be accessed without login or proper permissions
- [ ] Test session expiration and/or user logout functions work as expected (user should not be able to navigate further)
- [ ] Confirm that “View Source Code” is disabled for the end user
- [ ] Confirm that all cookies are stored in an encrypted format
- [ ] Confirm that cookies are not storing passwords
- Check set password/change password/forgot password
   - [ ] Submit properly and can be used moving forward
   - [ ] Old passwords no longer work once changed
- [ ] Check for any restricted site access if login attempts exceeds allowable number of attempts

## OTHER
- [ ] Site redirects
- [ ] Canonical target page
- Check Google Search console or Google Analytics for 404 errors 
   - [ ] In Analytics navigate to Behaviors > all content > sort using page title > search 404 and it will return a list of pages 
   - [ ] In Search Console navigate to diagnostics > crawl errors > not found and you will see list of pages 
- [Lighthouse audits](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk?hl=en): (see what areas can be improved)
   - [ ] Performance, SCORE: _____
   - [ ] Best Practices, SCORE: _____
   - [ ] SEO, SCORE: _____
   - [ ] Progressive Web App (PWA), SCORE: _____



