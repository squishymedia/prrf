Please make sure that each applicable task below is addressed. Once the task has been addressed, check off the box next to that task. As well, check off any that are not applicable to this ticket.
- [ ] **Regression testing**
- [ ] **Validation of client desire**

# Analytics Checklist
- [ ] **Google Analytics (or similar) and relevant analytics/tracking tools installed**
- [ ] **Analytics are enabled with correct credentials**

# Functionality Checklist
- [ ] **Code check**: readability, accuracy, removal of superfluous or dead-code
- [ ] **Site calls work as expected/intended** (verification)
- [ ] **Full test suite implemented and passing** 
   - [ ] Not applicable
- [ ] **Integration tests**
   - [ ] Not applicable
- [ ] **Site links resolve correctly** (e.g. links and PDFs open in a new tab)
   - [ ] Not applicable
- [ ] **Site forms resolve and flow properly**
   - [ ] Not applicable
- [ ] **Cookies work properly**
   - [ ] Not applicable
- [ ] **Search works properly** (e.g. relevance of results, displays number of results found)
   - [ ] Not applicable
- [ ] **Site files and database have been backed up and stored in a safe place**
   - [ ] Not applicable
- [ ] **Database indexing is correct**
   - [ ] Not applicable
- [ ] **“404 page” exists and provides helpful information for the user**
   - [ ] Not applicable
- [ ] **All external links are valid**
   - [ ] Not applicable

# Performance Checklist
- [ ] **Page load time**
   - Test tool(s) used: 
   - [ ] Not applicable                                    
- [ ] **Load testing**
   - Test tool(s) used: 
   - [ ] Not applicable
- [ ] **Stress testing**
   - Test tool(s) used: 
   - [ ] Not applicable
- [ ] **Image optimization**
   - [ ] Not applicable
- [ ] **Checked caching** (where necessary)
   - [ ] Not applicable
- [ ] **Checked web server configurations**
   - [ ] Not applicable
- [ ] **Checked database configurations**
   - [ ] Not applicable
- [ ] **Checked configurations of load balancers**
   - [ ] Not applicable
- [ ] **Checked configurations of server/application containers**
   - [ ] Not applicable
- [ ] **Canonical domain issues have been reduced to a single, consistent style**
- [ ] **Implemented automated tests** (e.g.  Selenium)
  - [ ] Not applicable
- [ ] **Disk space/capacity has been confirmed**
   - Current capacity is:  
   - [ ] Not applicable 

# Security Checklist
- [ ] **SSL certificate requested**
   - [ ] Not applicable
- [ ] **SSL certificate implemented**
   - [ ] Not applicable
- [ ] **User ID = 1 was removed from the database**
   - [ ] Not applicable
   - [ ] Will be checked during final sprint review 
- [ ] **Backup schedule is configured**
   - [ ] Not applicable
- [ ] **Recovery from backup has been tested**
   - [ ] Not applicable
- [ ] **Security/penetration tested**
   - [ ] Not applicable
- [ ] **Email is setup**
   - [ ] Not applicable
- [ ] **SMS monitoring and alerts are setup**
   - [ ] Not applicable

# SEO Checklist
- [ ] **Optimized with page titles**
- [ ] **Optimized with keywords**
- [ ] **Optimized with meta descriptions**
- [ ] **“301 redirects” have been used for any URLs that were removed**
   - [ ] Not applicable
- [ ] **Pages are descriptive and SEO friendly**
- [ ] **Add robot.txt and sitemap.xml**

# User Interface Checklist
- [ ] **Formatting and design are consistent throughout the site**
- [ ] **Design is responsive**
- [ ] **The *favicon* has been created and displays correctly** (retina included) 
- [ ] **Site meets the intention and general design of the mockup/wire/comp**
- [ ] **Links and buttons stand out**
- [ ] **Works in all major browsers and on required platforms**
- [ ] **Checked logos and other key images on retina displays**
- [ ] **ADA compliant**
- [ ] **All the sample data and dummy text have been removed**
- [ ] **Text is free from spelling errors** (not applicable for client responsible content)
- [ ] **All pages have content**
- [ ] **Grammar check**  (not applicable for client responsible content)
- [ ] **Checked headings for potential use of ligatures**
- [ ] **Resolved any/all orphans**
- [ ] **Consistent and correct tense:** present, past, future  (not applicable for client responsible content)
- [ ] **Consistent variation of words**
- [ ] **Treatment of bulleted lists** (periods, etc. should be consistent)
- [ ] **Resolved hard-coded links to staging domain** 
- [ ] **Checked all ‘hidden copy’** 



