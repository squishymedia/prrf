#!/bin/bash
# There are two variants of this script - changes should be applied to both
# See helm/db-setup.sh for the Kubernetes version
set -e

# create users and db
psql -v ON_ERROR_STOP=1 --username "${POSTGRES_USER}" --dbname "${POSTGRES_DB}" <<-EOSQL
    CREATE USER ${PGUSER} PASSWORD '${PGPASSWORD}';
    GRANT ALL PRIVILEGES ON DATABASE ${POSTGRES_DB} TO ${PGUSER};
    CREATE EXTENSION IF NOT EXISTS pgcrypto;
    CREATE SCHEMA IF NOT EXISTS hdb_catalog;
    CREATE SCHEMA IF NOT EXISTS hdb_views;
    ALTER SCHEMA hdb_catalog OWNER TO ${PGUSER};
    ALTER SCHEMA hdb_views OWNER TO ${PGUSER};
    GRANT SELECT ON ALL TABLES IN SCHEMA information_schema TO ${PGUSER};
    GRANT SELECT ON ALL TABLES IN SCHEMA pg_catalog TO ${PGUSER};
    GRANT USAGE ON SCHEMA public TO ${PGUSER};
    GRANT ALL ON ALL TABLES IN SCHEMA public TO ${PGUSER};
    GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO ${PGUSER};
    GRANT ALL ON ALL FUNCTIONS IN SCHEMA public TO ${PGUSER};
EOSQL

# load data
psql --username "${POSTGRES_USER}" "${POSTGRES_DB}" < /backups/prrf_07-20-2021_2.bak

