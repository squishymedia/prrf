#!/bin/sh

# migrate data from https://docs.google.com/spreadsheets/d/1JAXof89IIsJdn2CdA4HzVY7oWe9KFgeGtiVKozMYm7c/edit#gid=0
node ./services/db/load run

# add search objects
psql -v ON_ERROR_STOP=1 --username "${POSTGRES_USER}" --dbname "${POSTGRES_DB}" <<-EOSQL
    CREATE EXTENSION IF NOT EXISTS pg_trgm;

    SELECT set_limit(.05);

    CREATE INDEX IF NOT EXISTS cas_number_gin_idx ON pesticide
        USING GIN (cas_number gin_trgm_ops);
    CREATE INDEX IF NOT EXISTS name_gin_idx ON pesticide
        USING GIN ((name) gin_trgm_ops);

    DROP FUNCTION IF EXISTS search_text(search text);
    CREATE FUNCTION search_text(search text) 
        returns setof pesticide AS \$\$
        SELECT * FROM pesticide 
            WHERE search % ( name )
            OR search % ( cas_number )
            OR name ILIKE '%' || search || '%'
            OR cas_number ILIKE '%' || search || '%'
            ORDER BY similarity(search, ( name ));
    \$\$ language sql stable;
EOSQL

# run unit and integrity tests
yarn test ./services/db/__tests__/*.test.js
