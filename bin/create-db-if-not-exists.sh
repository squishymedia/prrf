#! /bin/bash

# https://gitlab.com/squishymedia/DOI/safecom-relaunch/-/wikis/home
#   importing data

# requires assets/Safecom_*.sql dump file adjacent to script file

# Usage:
#   bin directory with this file and assets/ can located wherever, but I put it in ~/
#   run from safecom root directory `~/bin/safecom_db_up.bash`

pwd

echo "Ensuring database prrf exists..."

echo "shutting down systems and running db..."
docker-compose stop
docker-compose up -d --remove-orphans db

echo "creating prrf if nonexistant..."
until docker-compose exec db bash -c "
psql -U postgres -d postgres << EOF

  SELECT 'CREATE DATABASE prrf'
  WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'prrf')\gexec

EOF
"
do
  echo "trying again..."
  sleep 3
done