# Pesticides_Risk_Reduction

## Getting Started
1. Create .env file
2. Run `git lfs pull` (you may need to [install git-lfs](https://docs.github.com/en/free-pro-team@latest/github/managing-large-files/installing-git-large-file-storage))
3. `yarn`
4. `docker-compose up`
5. Navigate to http://localhost:8080/console/data/schema/public and track all untracked tables, views, foreign-key relations, and functions

## Development
Packages and services are dockerized and can be run with `yarn develop` or `docker-compose up [-d]`.

### Packages and Services
Managed by Lerna via Yarn workspaces.
Adding dependencies to an individual package should be done from that package.

### Gatsby Site
Site at `http://localhost:8000`
GraphQL interactive playground at `http://localhost:8000/___graphql`

### Hasura (GraphQL Engine)
Console at `http://localhost:8080/console`

### Postgres Database
Accessible via terminal using the following command:
```
docker-compose exec postgres psql -U postgres
```

#### Updating the Database

Updates to the data are made in the [PRR Tables google sheet](https://docs.google.com/spreadsheets/d/1BRq96ru2vM1Q-Ta6eP4HOpO3L52vXJ3cdhhjIUsWUaE/edit#gid=0). To update the database:

1. run `docker-compose up -d db`
2. access the postgres shell with `docker-compose exec db psql -U postgres -d postgres`
3. in the postgres shell, run `DROP DATABASE prrf; CREATE DATABASE prrf;`
4. exit the postgres shell
5. run './bin/load-db.sh'
6. create a .bak file of the database (pg_dump works well) named with the current date `prrf_<MM-DD-YYYY>.bak` and put it in `./services/db/backups`. 
  - DO NOT REPLACE OLD DUMP FILES.
  - pg_dump command is `pg_dump -f /path/to/db/backups/prrf_<MM-DD-YYYY>.bak postgresql://<POSTGRES_SUPERUSER>:<POSTGRES_PASSWORD>@localhost:<LOAD_DB_PORT>/prrf` (variables in `<>` can be found in `/.env`)
7. update the dump file dates in `./helm/db-setup.sh` and `./bin/db-setup.sh`
8. run `docker-compose stop && docker-compose rm && docker-compose build && docker-compose up`
9. go to the [Hasura dashboard](http://localhost:8080/console) (note: if this does not work because of a permission failure, you may have to run the sql queries in ./bin/db-setup.sh in the prrf psql shell)
10. in the Data tab, track all "Untracked tables or views", "Untracked foreign-key relations", and "Untracked custom functions". You will likely need to do these one by one. "Untracked custom functions" may not be visible until the other two are tracked and the page is refreshed.
11. if the database tables, relationships, or functions have been changed, go to the Hasura Metadata Actions page (settings gear in the header, top-right) and `Export metadata`. Replace the contents of ./services/db/backups/hasura_metadata.json with the downloaded file contents.


### Testing
`yarn test`
