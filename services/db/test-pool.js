const { Pool } = require('pg')

module.exports = new Pool({
  connectionString: 'postgres://test-user@localhost:5433/test-db'
})