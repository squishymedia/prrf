-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.3-alpha1
-- PostgreSQL version: 12.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: new_database | type: DATABASE --
-- -- DROP DATABASE IF EXISTS new_database;
-- CREATE DATABASE new_database;
-- -- ddl-end --
-- 

-- object: public.risk_table_code | type: TYPE --
DROP TYPE IF EXISTS public.risk_table_code CASCADE;
CREATE TYPE public.risk_table_code AS
 ENUM ('1','2','3a','3b');
-- ddl-end --

-- object: public.pesticide_type | type: TABLE --
DROP TABLE IF EXISTS public.pesticide_type CASCADE;
CREATE TABLE public.pesticide_type (
	id smallint NOT NULL,
	type text NOT NULL,
	CONSTRAINT pesticide_type_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: public.chemical_classification | type: TABLE --
DROP TABLE IF EXISTS public.chemical_classification CASCADE;
CREATE TABLE public.chemical_classification (
	id smallint NOT NULL,
	class text,
	sub_class text,
	CONSTRAINT chemical_classification_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: public.mode_of_action | type: TABLE --
DROP TABLE IF EXISTS public.mode_of_action CASCADE;
CREATE TABLE public.mode_of_action (
	id smallint NOT NULL,
	term text,
	code text,
	CONSTRAINT mode_of_action_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: public.pesticide | type: TABLE --
DROP TABLE IF EXISTS public.pesticide CASCADE;
CREATE TABLE public.pesticide (
	id serial NOT NULL,
	cas_number text,
	name text,
	risk_table public.risk_table_code,
	id_pesticide_type smallint,
	id_chemical_classification smallint,
	id_mode_of_action smallint,
	obsolete bool,
	CONSTRAINT pesticide_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: pesticide_type_fk | type: CONSTRAINT --
ALTER TABLE public.pesticide DROP CONSTRAINT IF EXISTS pesticide_type_fk CASCADE;
ALTER TABLE public.pesticide ADD CONSTRAINT pesticide_type_fk FOREIGN KEY (id_pesticide_type)
REFERENCES public.pesticide_type (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;

-- object: chemical_classification_fk | type: CONSTRAINT --
ALTER TABLE public.pesticide DROP CONSTRAINT IF EXISTS chemical_classification_fk CASCADE;
ALTER TABLE public.pesticide ADD CONSTRAINT chemical_classification_fk FOREIGN KEY (id_chemical_classification)
REFERENCES public.chemical_classification (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;

-- object: mode_of_action_fk | type: CONSTRAINT --
ALTER TABLE public.pesticide DROP CONSTRAINT IF EXISTS mode_of_action_fk CASCADE;
ALTER TABLE public.pesticide ADD CONSTRAINT mode_of_action_fk FOREIGN KEY (id_mode_of_action)
REFERENCES public.mode_of_action (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;

-- object: public.hazards | type: TABLE --
DROP TABLE IF EXISTS public.hazards CASCADE;
CREATE TABLE public.hazards (
	id serial NOT NULL,
	who_1a bool,
	who_1b bool,
	ghs_cancer_1ab bool,
	ghs_mutagenic_1ab bool,
	ghs_reproductive_1ab bool,
	montreal_protocol bool,
	rotterdam_convention bool,
	stockholm_convention bool,
	severe_effects bool,
	id_pesticide integer NOT NULL,
	CONSTRAINT hazards_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: pesticide_fk | type: CONSTRAINT --
ALTER TABLE public.hazards DROP CONSTRAINT IF EXISTS pesticide_fk CASCADE;
ALTER TABLE public.hazards ADD CONSTRAINT pesticide_fk FOREIGN KEY (id_pesticide)
REFERENCES public.pesticide (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: hazards_uq | type: CONSTRAINT --
ALTER TABLE public.hazards DROP CONSTRAINT IF EXISTS hazards_uq CASCADE;
ALTER TABLE public.hazards ADD CONSTRAINT hazards_uq UNIQUE (id_pesticide);
-- ddl-end --

-- object: public.risk_mitigations | type: TABLE --
DROP TABLE IF EXISTS public.risk_mitigations CASCADE;
CREATE TABLE public.risk_mitigations (
	id serial NOT NULL,
	better_personal_protection bool,
	aquatic bool,
	wildlife bool,
	pollinator bool,
	bystander bool,
	id_pesticide integer NOT NULL,
	CONSTRAINT risk_mitigations_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: risk_mitigations_pk1 | type: CONSTRAINT --
ALTER TABLE public.risk_mitigations DROP CONSTRAINT IF EXISTS risk_mitigations_pk1 CASCADE;
ALTER TABLE public.risk_mitigations ADD CONSTRAINT risk_mitigations_pk1 FOREIGN KEY (id_pesticide)
REFERENCES public.pesticide (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: risk_mitigations_uq | type: CONSTRAINT --
ALTER TABLE public.risk_mitigations DROP CONSTRAINT IF EXISTS risk_mitigations_uq CASCADE;
ALTER TABLE public.risk_mitigations ADD CONSTRAINT risk_mitigations_uq UNIQUE (id_pesticide);
-- ddl-end --

-- object: public.target_pest | type: TABLE --
DROP TABLE IF EXISTS public.target_pest CASCADE;
CREATE TABLE public.target_pest (
	id smallint NOT NULL,
	common_name text,
	"order" text,
	species text,
	CONSTRAINT target_pest_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: public.natural_enemy | type: TABLE --
DROP TABLE IF EXISTS public.natural_enemy CASCADE;
CREATE TABLE public.natural_enemy (
	id smallint NOT NULL,
	organism text NOT NULL,
	CONSTRAINT natural_enemy_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: public.many_pesticide_has_many_pesticide_type | type: TABLE --
DROP TABLE IF EXISTS public.many_pesticide_has_many_pesticide_type CASCADE;
CREATE TABLE public.many_pesticide_has_many_pesticide_type (
	id_pesticide integer NOT NULL,
	id_pesticide_type smallint NOT NULL,
	CONSTRAINT pesticide_pesticide_type_fk PRIMARY KEY (id_pesticide,id_pesticide_type)

);
-- ddl-end --

-- object: many_pesticide_has_many_pesticide_type_pk | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_pesticide_type DROP CONSTRAINT IF EXISTS many_pesticide_has_many_pesticide_type_pk CASCADE;
ALTER TABLE public.many_pesticide_has_many_pesticide_type ADD CONSTRAINT many_pesticide_has_many_pesticide_type_pk FOREIGN KEY (id_pesticide)
REFERENCES public.pesticide (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: many_pesticide_has_many_pesticide_type_uq | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_pesticide_type DROP CONSTRAINT IF EXISTS many_pesticide_has_many_pesticide_type_uq CASCADE;
ALTER TABLE public.many_pesticide_has_many_pesticide_type ADD CONSTRAINT many_pesticide_has_many_pesticide_type_uq FOREIGN KEY (id_pesticide_type)
REFERENCES public.pesticide_type (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.many_pesticide_has_many_chemical_classification | type: TABLE --
DROP TABLE IF EXISTS public.many_pesticide_has_many_chemical_classification CASCADE;
CREATE TABLE public.many_pesticide_has_many_chemical_classification (
	id_pesticide integer NOT NULL,
	id_chemical_classification smallint NOT NULL,
	CONSTRAINT pesticide_chemical_classification_fk PRIMARY KEY (id_pesticide,id_chemical_classification)

);
-- ddl-end --

-- object: many_pesticide_has_many_chemical_classification_pk | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_chemical_classification DROP CONSTRAINT IF EXISTS many_pesticide_has_many_chemical_classification_pk CASCADE;
ALTER TABLE public.many_pesticide_has_many_chemical_classification ADD CONSTRAINT many_pesticide_has_many_chemical_classification_pk FOREIGN KEY (id_pesticide)
REFERENCES public.pesticide (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: many_pesticide_has_many_chemical_classification_uq | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_chemical_classification DROP CONSTRAINT IF EXISTS many_pesticide_has_many_chemical_classification_uq CASCADE;
ALTER TABLE public.many_pesticide_has_many_chemical_classification ADD CONSTRAINT many_pesticide_has_many_chemical_classification_uq FOREIGN KEY (id_chemical_classification)
REFERENCES public.chemical_classification (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.many_pesticide_has_many_mode_of_action | type: TABLE --
DROP TABLE IF EXISTS public.many_pesticide_has_many_mode_of_action CASCADE;
CREATE TABLE public.many_pesticide_has_many_mode_of_action (
	id_pesticide integer NOT NULL,
	id_mode_of_action smallint NOT NULL,
	CONSTRAINT pesticide_mode_of_action_fk PRIMARY KEY (id_pesticide,id_mode_of_action)

);
-- ddl-end --

-- object: many_pesticide_has_many_mode_of_action_pk | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_mode_of_action DROP CONSTRAINT IF EXISTS many_pesticide_has_many_mode_of_action_pk CASCADE;
ALTER TABLE public.many_pesticide_has_many_mode_of_action ADD CONSTRAINT many_pesticide_has_many_mode_of_action_pk FOREIGN KEY (id_pesticide)
REFERENCES public.pesticide (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: many_pesticide_has_many_mode_of_action_uq | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_mode_of_action DROP CONSTRAINT IF EXISTS many_pesticide_has_many_mode_of_action_uq CASCADE;
ALTER TABLE public.many_pesticide_has_many_mode_of_action ADD CONSTRAINT many_pesticide_has_many_mode_of_action_uq FOREIGN KEY (id_mode_of_action)
REFERENCES public.mode_of_action (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.many_target_pest_has_many_natural_enemy | type: TABLE --
DROP TABLE IF EXISTS public.many_target_pest_has_many_natural_enemy CASCADE;
CREATE TABLE public.many_target_pest_has_many_natural_enemy (
	id_target_pest smallint NOT NULL,
	id_natural_enemy smallint NOT NULL,
	CONSTRAINT target_pest_natural_enemy_fk PRIMARY KEY (id_target_pest,id_natural_enemy)

);
-- ddl-end --

-- object: many_target_pest_has_many_natural_enemy_pk | type: CONSTRAINT --
ALTER TABLE public.many_target_pest_has_many_natural_enemy DROP CONSTRAINT IF EXISTS many_target_pest_has_many_natural_enemy_pk CASCADE;
ALTER TABLE public.many_target_pest_has_many_natural_enemy ADD CONSTRAINT many_target_pest_has_many_natural_enemy_pk FOREIGN KEY (id_target_pest)
REFERENCES public.target_pest (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: many_target_pest_has_many_natural_enemy_uq | type: CONSTRAINT --
ALTER TABLE public.many_target_pest_has_many_natural_enemy DROP CONSTRAINT IF EXISTS many_target_pest_has_many_natural_enemy_uq CASCADE;
ALTER TABLE public.many_target_pest_has_many_natural_enemy ADD CONSTRAINT many_target_pest_has_many_natural_enemy_uq FOREIGN KEY (id_natural_enemy)
REFERENCES public.natural_enemy (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.many_natural_enemy_has_many_pesticide | type: TABLE --
DROP TABLE IF EXISTS public.many_natural_enemy_has_many_pesticide CASCADE;
CREATE TABLE public.many_natural_enemy_has_many_pesticide (
	id_natural_enemy smallint NOT NULL,
	id_pesticide integer NOT NULL,
	toxicity text,
	mitigation_required bool,
	notes text,
	CONSTRAINT natural_enemy_fk PRIMARY KEY (id_natural_enemy,id_pesticide)

);
-- ddl-end --

-- object: many_natural_enemy_has_many_pesticide_pk | type: CONSTRAINT --
ALTER TABLE public.many_natural_enemy_has_many_pesticide DROP CONSTRAINT IF EXISTS many_natural_enemy_has_many_pesticide_pk CASCADE;
ALTER TABLE public.many_natural_enemy_has_many_pesticide ADD CONSTRAINT many_natural_enemy_has_many_pesticide_pk FOREIGN KEY (id_natural_enemy)
REFERENCES public.natural_enemy (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: many_natural_enemy_has_many_pesticide_uq | type: CONSTRAINT --
ALTER TABLE public.many_natural_enemy_has_many_pesticide DROP CONSTRAINT IF EXISTS many_natural_enemy_has_many_pesticide_uq CASCADE;
ALTER TABLE public.many_natural_enemy_has_many_pesticide ADD CONSTRAINT many_natural_enemy_has_many_pesticide_uq FOREIGN KEY (id_pesticide)
REFERENCES public.pesticide (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.many_pesticide_has_many_target_pest | type: TABLE --
DROP TABLE IF EXISTS public.many_pesticide_has_many_target_pest CASCADE;
CREATE TABLE public.many_pesticide_has_many_target_pest (
	id_pesticide integer NOT NULL,
	id_target_pest smallint NOT NULL,
	CONSTRAINT pesticide_fk PRIMARY KEY (id_pesticide,id_target_pest)

);
-- ddl-end --

-- object: many_pesticide_has_many_target_pest_pk | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_target_pest DROP CONSTRAINT IF EXISTS many_pesticide_has_many_target_pest_pk CASCADE;
ALTER TABLE public.many_pesticide_has_many_target_pest ADD CONSTRAINT many_pesticide_has_many_target_pest_pk FOREIGN KEY (id_pesticide)
REFERENCES public.pesticide (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: many_pesticide_has_many_target_pest_uq | type: CONSTRAINT --
ALTER TABLE public.many_pesticide_has_many_target_pest DROP CONSTRAINT IF EXISTS many_pesticide_has_many_target_pest_uq CASCADE;
ALTER TABLE public.many_pesticide_has_many_target_pest ADD CONSTRAINT many_pesticide_has_many_target_pest_uq FOREIGN KEY (id_target_pest)
REFERENCES public.target_pest (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --


