const { Pool } = require('pg')

const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') })

module.exports = new Pool({
  user: process.env.PGUSER,
  host: process.env.LOAD_DB_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.PGPASSWORD,
  port: process.env.LOAD_DB_PORT,
})
