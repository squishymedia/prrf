const { getValues } = require('./gsheets')
const { 
  pesticideType_gid, pesticideTypeTableHeadersToColumns,
  chemClass_gid, chemClassTableHeadersToColumns,
  mode_gid, modeTableHeadersToColumns,
  target_gid, targetTableHeadersToColumns,
  naturalEnemy_gid, naturalEnemyTableHeadersToColumns,
  
  table1_gid, table2_gid, table3a_gid, table3b_gid,
  t1TableHeadersToColumns, t1ColumnsToTable, t1AdditionalColumns,
  t2TableHeadersToColumns, t2ColumnsToTable, t2AdditionalColumns,
  t3aTableHeadersToColumns, t3aColumnsToTable, t3aAdditionalColumns,
  t3bTableHeadersToColumns, t3bColumnsToTable, t3bAdditionalColumns,
  
  pest_Target_gid, pest_TargetHeadersToColumns,
  target_NatEnemy_gid, target_NatEnemyHeadersToColumns,
  pest_NatEnemy_gid, pest_NatEnemyHeadersToColumns,
  booleanFields, stringFields,
} = require('./config')
const { pesticideIDs } = require('./id-cache')

/**
 * handle notes to side of google sheet tables
 * @param {*} headers 
 * @param {*} data 
 */
const trimNotes = (headers, data) => {
  let width = headers.length
  return data.map(row => row.slice(0, width))
}

/**
 * adds additional columns describe in config
 * @param {*} headers 
 * @param {*} data 
 * @param {*} additionalColumns 
 */
const addAdditionalColumns = (headers, data, additionalColumns) => {
  headers = [...headers, ...additionalColumns]
  data = data.map(row => {
    row.length = headers.length
    return row
  })
  return [headers, data]
}

/**
 * ensure that columns expected to be boolean are so
 * @param {*} data 
 * @param {*} headers 
 */
const handleBooleans = (headers, data) => data.map(row =>
  headers.map((header, i) => 
    booleanFields.includes(header)
      ? Boolean(row[i])
      : row[i]
  )
)

/**
 * sets the risk_table field for each row to desired value
 * @param {*} headers 
 * @param {*} data 
 * @param {*} riskTable 
 */
const setRiskTable = (headers, data, riskTable) => {
  const riskTableColumnIndex = headers.findIndex(header => header ==='risk_table')
  return data.map(row => {
    row[riskTableColumnIndex] = riskTable
    return row
  })
}

const formatTable1 = t1 => {
  let [ _, headers, ...data ] = t1.values
  headers = headers.map(tableHeader => t1TableHeadersToColumns[tableHeader])
  
  data = trimNotes(headers, data)
  ;([headers, data] = addAdditionalColumns(headers, data, t1AdditionalColumns))
  data = setRiskTable(headers, data, '1')

  data = handleBooleans(headers, data)

  return { headers, data }
}

const formatPesticideTable = (tableCode, values, tableHeadersToColumns, additionalColumns) => {
  let [ _, headers, ...data ] = values.values
  headers = headers.map(tableHeader => tableHeadersToColumns[tableHeader])
 
  data = trimNotes(headers, data)
  ;([headers, data] = addAdditionalColumns(headers, data, additionalColumns))
  data = setRiskTable(headers, data, tableCode)
  data = handleBooleans(headers, data)
  
  return { headers, data }
}

const formatSimpleTable = (table, tableHeadersToColumns, riskTable, additionalColumns=[]) => {
  let [ _, headers, ...data ] = table.values
  headers = headers.map(tableHeader => tableHeadersToColumns[tableHeader])
  
  data = trimNotes(headers, data)
  if (additionalColumns.length)
    ;([headers, data] = addAdditionalColumns(headers, data, additionalColumns))
  data = handleBooleans(headers, data)
  if (riskTable)
    data = setRiskTable(headers, data, riskTable)

  // handle strings edge-cases
  data = data.map(row =>
    headers.map((header, i) => {
      if (!stringFields.includes(header)) return row[i]
      if (!row[i]) return ''
      if (typeof row[i] === 'string' && row[i].includes('\'')) 
        return row[i].replace('\'', '\'\'')
      return String(row[i])
    })
  )

  return { headers, data }
}

/**
 * Sets the field for a specified column on the given row
 * @param {*} queryData 
 * @param {*} header 
 * @param {*} value 
 */
const setColumn = (queryData, header, value) => {
  const { headers, data } = queryData
  const i = headers.findIndex(h => h === header)
  data[i] = value
}

/**
 * Uses the columnsToTable mappers in ./config 
 *   to sort the given headers and data
 *   into multiple packages, each consisting of
 *   - the target table
 *   - the column names
 *   - the rows for those columns
 * @param {*} headers 
 * @param {*} data 
 * @param {*} columnsToTable 
 */
const splitData = (headers, data, columnsToTable) => {
  const targetTables = Array.from(new Set(Object.values(columnsToTable).flat()))
  const templateQueryDataPackages = {}
  targetTables.forEach(table => {
    const queryData = {
      table,
      headers: Object.entries(columnsToTable)
        .filter(([_, targetTable]) => Array.isArray(targetTable) ? targetTable.includes(table) : targetTable === table)
        .map(([header]) => header),
      headerIndices: {},  
      data: [],
    }
    queryData.headers.forEach((header, i) => {
      queryData.headerIndices[header] = i
    })
    templateQueryDataPackages[table] = queryData
  })
  function QueryDataPackages() {
    const self = this
    Object.entries(templateQueryDataPackages)
      .forEach(([table, queryData]) => {
        self[table] = {
          ...queryData,
          data: []
        }
      })
  }

  const splitData = data.map(row => {
    const queryDataPackages = new QueryDataPackages()
    row.forEach((value, i) => {
      const header = headers[i]
      // skip columns not listed in config
      if (!header) return queryDataPackages
      const tables = columnsToTable[header]
      if (Array.isArray(tables)) {
        tables.forEach(table => {
          const queryData = queryDataPackages[table]
          queryData.data[queryData.headerIndices[header]] = value
        })
      } else {
        const table = tables
        const queryData = queryDataPackages[table]
        queryData.data[queryData.headerIndices[header]] = value
      }
    })
    delete queryDataPackages.headerIndices
    return queryDataPackages
  })
  return splitData
}

/**
 * Convert JS type to postgresQL equivalent
 * @param {*} value 
 */
const formatType = value => {
  if (value === '') return undefined
  else if (typeof value === 'string') return `${value}`
  else if (typeof value === 'boolean') return value
  else if (typeof value === 'number') return value
  else return value
}

function* sequentialParameters() {
  let i = 1
  while (true) yield `$${i++}`
}

/**
 * Constructs a sql query's text and values given a target table, columns, and rows
 * @param {*} table target
 * @param {*} headers columns
 * @param {*} data rows
 */
const buildBulkInsertQuery = (table, headers, data) => {
  const n = sequentialParameters()
  const text = `
INSERT INTO ${table} 
(${headers.join(', ')})
VALUES 
${data.map(row => `(${row.map(() => n.next().value).join(', ')})`).join(',\n')};`
  const values = data.reduce((values, row) => {
    return [
      ...values,
      ...row.map(formatType)
    ]
  }, [])
  return [text, values]
}

/**
 * Constructs a sql query given a target table, columns, and data
 * returns inserted row
 * @param {*} param0 
 */
const buildRowInsertQuery = ({ table, headers, data }) => {
  const n = sequentialParameters()
  const text = `
INSERT INTO ${table} 
(${headers.join(', ')})
VALUES 
${`(${data.map(() => n.next().value).join(', ')})`}
RETURNING *;
`
  const values = data.map(formatType)
  return [text, values]
}

// fk: foreign key, assumed to start with 'id_' (eg. 'id_pesticide')
const idPattern = /^id_(.*)$/
const getM2MTableName = (fk1, fk2) => {
  [table1Name, table2Name] = [fk1, fk2].map(key => key.match(idPattern)[1])
  return `many_${table1Name}_has_many_${table2Name}`
}

// TODO?: (david) build query to insert related data to multiple tables in bulk
//   do if amount of data in tables becomes large
//   good resource: https://stackoverflow.com/questions/20561254/insert-data-in-3-tables-at-a-time-using-postgres

/**
 * For adding simple M2M relationships to pesticide table
 * @param {*} pool 
 * @param {number} thisID the id of the (pesticide) with the related rows
 * @param {string} thatField field name of the related table foreign key (eg. id_mode_of_action)
 * @param {number[]} thoseIDs the ids of the related rows (eg. rows on mode_of_action)
 * @param {string} thisField defaults to id_pesticide
 */
const addBasicM2MRelationships = (pool, thisID, thatField, thoseIDs, thisField = 'id_pesticide', ) => {
  thoseIDs = typeof thoseIDs === 'string' && thoseIDs 
    ? thoseIDs.split(',').map(id => parseInt(id)) 
    : null
  if (thoseIDs) {
    const headers = ['id_pesticide', thatField]
    const data = thoseIDs.map(that_id => [thisID, that_id])
    const [text, values] = buildBulkInsertQuery(
      getM2MTableName(thisField, thatField),
      headers,
      data
    )
    return pool.query(text, values)
  }
}

const migratePesticideType = async pool => {
  const pesticideTypeTable = await getValues(pesticideType_gid)
  const { headers, data } = formatSimpleTable(
    pesticideTypeTable,
    pesticideTypeTableHeadersToColumns,
  )
  const [text, values] = buildBulkInsertQuery('pesticide_type', headers, data)
  await pool.query(text, values)
}

const migrateChemClass = async pool => {
  const chemClassTable = await getValues(chemClass_gid)
  const { headers, data } = formatSimpleTable(
    chemClassTable,
    chemClassTableHeadersToColumns,
  )
  const [text, values] = buildBulkInsertQuery('chemical_classification', headers, data)
  await pool.query(text, values)
}

const migrateMode = async pool => {
  const modeTable = await getValues(mode_gid)
  const { headers, data } = formatSimpleTable(
    modeTable,
    modeTableHeadersToColumns,
  )
  const [text, values] = buildBulkInsertQuery('mode_of_action', headers, data)
  await pool.query(text, values)
}

const migrateTarget = async pool => {
  const targetTable = await getValues(target_gid)
  const { headers, data } = formatSimpleTable(
    targetTable,
    targetTableHeadersToColumns,
  )
  const [text, values] = buildBulkInsertQuery('target_pest', headers, data)
  await pool.query(text, values)
}

const migrateNaturalEnemy = async pool => {
  const naturalEnemyTable = await getValues(naturalEnemy_gid)
  const { headers, data } = formatSimpleTable(
    naturalEnemyTable,
    naturalEnemyTableHeadersToColumns,
  )
  const [text, values] = buildBulkInsertQuery('natural_enemy', headers, data)
  await pool.query(text, values)
}

const migrateTable1 = async pool => {
  const table1 = await getValues(table1_gid)
  let { headers, data } = formatTable1(table1)
  data = splitData(headers, data, t1ColumnsToTable)
  await Promise.all(data.map(async (row, i) => {
    const [pesticideText, pesticideValues] = buildRowInsertQuery(row.pesticide)
    const res = await pool.query(pesticideText, pesticideValues)
    const { id } = res.rows[0]
    const sheetsPID = row.CONVERT_PESTICIDE_ID.data[0]
    pesticideIDs[sheetsPID] = id
    
    setColumn(row.hazards, 'id_pesticide', id)
    const [hazardsText, hazardsValues] = buildRowInsertQuery(row.hazards)
    const hazardsInsert = pool.query(hazardsText, hazardsValues)
    
    setColumn(row.risk_mitigations, 'id_pesticide', id)
    const [riskMitigationsText, riskMitigationsValues] = buildRowInsertQuery(row.risk_mitigations)
    const riskMitigationsInsert = pool.query(riskMitigationsText, riskMitigationsValues)
    
    const relationshipInserts = []
    if (row.RELATIONSHIP_KEY_M2M) {
      const relationships = row.RELATIONSHIP_KEY_M2M
      const pkFields = relationships.headers // pk: primary key
      relationshipInserts.concat(
        pkFields.map(pkField =>
          addBasicM2MRelationships(
            pool,
            id,
            pkField,
            relationships.data[relationships.headerIndices[pkField]]
          )
        )
      )
    }
    await Promise.all([
      hazardsInsert,
      riskMitigationsInsert,
      relationshipInserts,
    ])
  }))
}

const migrateTable2 = async pool => {
  const table2 = await getValues(table2_gid)
  let { headers, data } = formatPesticideTable('2', table2, t2TableHeadersToColumns, t2AdditionalColumns)
  data = splitData(headers, data, t2ColumnsToTable)
  await Promise.all(data.map(async row => {
    const [pesticideText, pesticideValues] = buildRowInsertQuery(row.pesticide)
    const res = await pool.query(pesticideText, pesticideValues)
    const { id } = res.rows[0]
    const sheetsPID = row.CONVERT_PESTICIDE_ID.data[0]
    pesticideIDs[sheetsPID] = id

    setColumn(row.risk_mitigations, 'id_pesticide', id)
    const [riskMitigationsText, riskMitigationsValues] = buildRowInsertQuery(row.risk_mitigations)
    const riskMitigationsInsert = pool.query(riskMitigationsText, riskMitigationsValues)

    const relationshipInserts = []
    if (row.RELATIONSHIP_KEY_M2M) {
      const relationships = row.RELATIONSHIP_KEY_M2M
      const pkFields = relationships.headers // pk: primary key
      relationshipInserts.concat(
        pkFields.map(pkField =>
          addBasicM2MRelationships(
            pool,
            id,
            pkField,
            relationships.data[relationships.headerIndices[pkField]]
          )
        )
      )
    }
    await Promise.all([
      riskMitigationsInsert,
      relationshipInserts,
    ])
  }))
}

const migrateTable3a = async pool => {
  const table3a = await getValues(table3a_gid)
  let { headers, data } = formatPesticideTable('3a', table3a, t3aTableHeadersToColumns, t3aAdditionalColumns)
  data = splitData(headers, data, t3aColumnsToTable)
  await Promise.all(data.map(async row => {
    const [pesticideText, pesticideValues] = buildRowInsertQuery(row.pesticide)
    const res = await pool.query(pesticideText, pesticideValues)
    const { id } = res.rows[0]
    const sheetsPID = row.CONVERT_PESTICIDE_ID.data[0]
    pesticideIDs[sheetsPID] = id

    if (row.RELATIONSHIP_KEY_M2M) {
      const relationships = row.RELATIONSHIP_KEY_M2M
      const pkFields = relationships.headers // pk: primary key
      pkFields.forEach(pkField =>
        addBasicM2MRelationships(
          pool,
          id,
          pkField,
          relationships.data[relationships.headerIndices[pkField]]
        )
      )
    }
  }))
}

const migrateTable3b = async pool => {
  const table3b = await getValues(table3b_gid)
  let { headers, data } = formatPesticideTable('3b', table3b, t3bTableHeadersToColumns, t3bAdditionalColumns)
  data = splitData(headers, data, t3bColumnsToTable)
  await Promise.all(data.map(async row => {
    const [pesticideText, pesticideValues] = buildRowInsertQuery(row.pesticide)
    const res = await pool.query(pesticideText, pesticideValues)
    const { id } = res.rows[0]
    const sheetsPID = row.CONVERT_PESTICIDE_ID.data[0]
    pesticideIDs[sheetsPID] = id

    if (row.RELATIONSHIP_KEY_M2M) {
      const relationships = row.RELATIONSHIP_KEY_M2M
      const pkFields = relationships.headers // pk: primary key
      pkFields.forEach(pkField =>
        addBasicM2MRelationships(
          pool,
          id,
          pkField,
          relationships.data[relationships.headerIndices[pkField]]
        )
      )
    }
  }))
}

const updatePesticideIDs = (headers, data) => {
  const pesticideIdIndex = headers.findIndex(header => header === 'id_pesticide')
  return [
    headers, 
    data.map(row => {
      const databaseId = pesticideIDs[row[pesticideIdIndex]]
      if (!databaseId) {
        throw Error(`Pesticide with google sheet id ${row[pesticideIdIndex]} does not exist.`)
      }
      row[pesticideIdIndex] = databaseId
      return row
    })
  ]
}

const migratePesticide_Target = async pool => {
  const pest_TargetTable = await getValues(pest_Target_gid)
  let { headers, data } = formatSimpleTable(
    pest_TargetTable,
    pest_TargetHeadersToColumns,
  )
  ;([headers, data] = updatePesticideIDs(headers, data))
  const [text, values] = buildBulkInsertQuery('many_pesticide_has_many_target_pest', headers, data)
  await pool.query(text, values)
}

const migrateTarget_NaturalEnemy = async pool => {
  const target_NatEnemyTable = await getValues(target_NatEnemy_gid)
  let { headers, data } = formatSimpleTable(
    target_NatEnemyTable,
    target_NatEnemyHeadersToColumns,
  )
  const [text, values] = buildBulkInsertQuery('many_target_pest_has_many_natural_enemy', headers, data)
  await pool.query(text, values)
}

const migrateNaturalEnemy_Pesticide = async pool => {
  const pest_NatEnemyTable = await getValues(pest_NatEnemy_gid)
  let { headers, data } = formatSimpleTable(
    pest_NatEnemyTable,
    pest_NatEnemyHeadersToColumns,
  )
  ;([headers, data] = updatePesticideIDs(headers, data))
  const [text, values] = buildBulkInsertQuery('many_natural_enemy_has_many_pesticide', headers, data)
  await pool.query(text, values)
}

/**
 * Gets all tables. Used in integrity test.
 */
const getTables = async () => {
  const [ t1, t2, t3a, t3b ] = await Promise.all(
    [table1_gid, table2_gid, table3a_gid, table3b_gid]
    .map(id => getValues(id))
  )
  return { t1, t2, t3a, t3b }
}

module.exports = {
  migratePesticideType,
  migrateChemClass,
  migrateMode,
  migrateTarget,
  migrateNaturalEnemy,

  migrateTable1,
  migrateTable2,
  migrateTable3a,
  migrateTable3b,

  migratePesticide_Target,
  migrateTarget_NaturalEnemy,
  migrateNaturalEnemy_Pesticide,

  formatTable1,
  formatPesticideTable,
  formatSimpleTable,

  trimNotes,
  addAdditionalColumns,
  handleBooleans,
  setRiskTable,
  splitData,
  formatType,
  buildBulkInsertQuery,
  buildRowInsertQuery,

  getTables
}
