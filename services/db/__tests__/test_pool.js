const { Pool } = require('pg')

/**
 * 
 * @param {string} url - postgres database url of form postgres://<user>@<host>:<port>/<db-name>
 */
module.exports = connectionString => {
  return new Pool({
    connectionString,
    max: 10,
    idleTimeoutMillis: 100_000,
    connectionTimeoutMillis: 100_000,
  })
}
