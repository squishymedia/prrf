const getDatabase = require("@databases/pg-test").default
const path = require("path")
require("dotenv").config({ path: path.resolve(__dirname, "../.env") })
const getPool = require("./test_pool.js")
const pgSetup = require("@databases/pg-test/jest/globalSetup")
const pgTeardown = require("@databases/pg-test/jest/globalTeardown")

const pause = ms => new Promise(resolve => 
  setTimeout(() => resolve(), ms)
)

const run = async () => {
  await pgSetup()
  const { databaseURL, kill } = await getDatabase()
  console.log(databaseURL)
  const pool = getPool(databaseURL)
  console.log("**1**")
  let res = await pool.query(`
  SELECT current_database();
  `)
  console.log(res.rows)
  console.log("**2**")
  res = await pool.query(`
  SELECT current_database();
  `)
  console.log(res.rows)
  await pool.end()
  await pgTeardown()
}

run().catch(err => console.error(err))
