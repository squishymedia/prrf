const loadDatabase = require("../load")

const getPool = require("./test_pool.js")
require("../require-sql")
const { t1ColumnsToTable, t2ColumnsToTable } = require("../config")
const { getTables } = require("../migrate")

const getColumnsQuery = tableName => `
SELECT
  column_name
FROM
  information_schema.columns
WHERE
  table_name = '${tableName}';
`
const getColumnsInDBTable = async (pool, tableName) => {
  const query = getColumnsQuery(tableName)
  const response = await pool.query(query)
  const columns = response.rows.map(({ column_name }) => column_name)
  return columns
}

jest.setTimeout(30 * 1000)

const getExpectedColumns = tableName => {
  const allColumnsToTable = {
    ...t1ColumnsToTable,
    ...t2ColumnsToTable,
  }
  const columnsOnThisTable = Object.entries(allColumnsToTable)
    .filter(([column, table]) => table === tableName)
    .map(([column]) => column)
  const deDuplicatedColumns = Array.from(new Set(columnsOnThisTable))
  return deDuplicatedColumns
}

let pool

describe("migration integrity checks", () => {
  beforeAll(async done => {
    const setupPool = getPool(process.env.DATABASE_URL)
    await loadDatabase(setupPool, true)
    pool = getPool(process.env.DATABASE_URL)
    done()
  })
  afterAll(() => {
    return pool.end()
  })
  describe("pesticide table", () => {
    it("has the expected number of rows", async done => {
      const { t1, t2, t3a, t3b } = await getTables()
      let numRowsExpected = [t1, t2, t3a, t3b].reduce(
        (rowCount, gSheet) => rowCount + gSheet.values.length,
        0
      )
      // discount header rows (2 each)
      numRowsExpected -= 4 * 2

      const response = await pool.query(`
        SELECT COUNT(*) FROM pesticide;
      `)
      const numRowsInDB = Number(response.rows[0].count)
      expect(numRowsInDB).toEqual(numRowsExpected)
      done()
    })

    it("has the expected columns", async done => {
      const expectedColumns = getExpectedColumns("pesticide")
      const columns = await getColumnsInDBTable(pool, "pesticide")
      expectedColumns.forEach(expectedColumn => {
        expect(columns).toContain(expectedColumn)
      })
      done()
    })
  })

  describe("risk_mitigations table", () => {
    it("has the expected number of rows", async done => {
      const { t1, t2 } = await getTables()
      let numRowsExpected = t1.values.length + t2.values.length
      // discount headers
      numRowsExpected -= 4 // two header rows on both contributing tables

      const response = await pool.query(`
        SELECT COUNT(*) FROM risk_mitigations;
      `)
      const numRowsInDB = Number(response.rows[0].count)
      expect(numRowsInDB).toEqual(numRowsExpected)
      done()
    })

    it("has the expected columns", async done => {
      const expectedColumns = getExpectedColumns("risk_mitigations")
      const columns = await getColumnsInDBTable(pool, "risk_mitigations")
      expectedColumns.forEach(expectedColumn => {
        expect(columns).toContain(expectedColumn)
      })
      done()
    })
  })

  describe("hazards table", () => {
    it("has the expected number of rows", async done => {
      const { t1 } = await getTables()
      let numRowsExpected = t1.values.length
      // discount header
      numRowsExpected -= 1
      // discount obsolete pesticides header
      numRowsExpected -= 1

      const response = await pool.query(`
        SELECT COUNT(*) FROM hazards;
      `)
      const numRowsInDB = Number(response.rows[0].count)
      expect(numRowsInDB).toEqual(numRowsExpected)
      done()
    })

    it("has the expected columns", async done => {
      const expectedColumns = getExpectedColumns("hazards")
      const columns = await getColumnsInDBTable(pool, "hazards")
      expectedColumns.forEach(expectedColumn => {
        expect(columns).toContain(expectedColumn)
      })
      done()
    })
  })
})
