const pool = require('../pool.js')
require('../require-sql')
const { 
  t1TableHeadersToColumns, t1AdditionalColumns,
  t2TableHeadersToColumns, t2AdditionalColumns,
  pesticideTypeTableHeadersToColumns,
  booleanFields
} = require('../config')
const { 
  formatTable1, formatPesticideTable, formatSimpleTable,
  trimNotes, addAdditionalColumns, handleBooleans, setRiskTable, 
  splitData, formatType, buildBulkInsertQuery, buildRowInsertQuery,
} = require('../migrate.js')

const arrayOfLength = (n, contents='x') => Array.from({ length: n}, () => contents)
const expectEqualIgnoringOrder = (arr1, arr2, sortFunction) => 
  expect(arr1.map(x=>x).sort(sortFunction))
    .toEqual(arr2.map(x=>x).sort(sortFunction))

describe('migration unit tests', () => {
  describe('table reformatters', () => {
    describe('formatTable1()', () => {
      gsheetHeaders = Object.keys(t1TableHeadersToColumns)
      dbHeaders = [...gsheetHeaders, ...t1AdditionalColumns]
      const valuesResponse = {
        values: [
          'Title Header',
          gsheetHeaders,
          arrayOfLength(gsheetHeaders.length, 'row1'),
          arrayOfLength(gsheetHeaders.length, 'row2'),
          arrayOfLength(gsheetHeaders.length, 'row3'),
        ]
      }
      it('separates headers and data rows', () => {
        const results = formatTable1(valuesResponse)
        expect(results).toHaveProperty('headers')
  
        // headers, rows, and columns should have correct dimensions
        const t1DatabaseHeaders = [
          ...Object.values(t1TableHeadersToColumns),
          ...t1AdditionalColumns,
        ]
        expectEqualIgnoringOrder(results.headers, t1DatabaseHeaders)
        expect(results.data).toHaveLength(3)
        expect(results.data[0]).toHaveLength(t1DatabaseHeaders.length)
      })
    })
  
    describe('formatPesticideTable()', () => {
      gsheetHeaders = Object.keys(t2TableHeadersToColumns)
      dbHeaders = [...gsheetHeaders, ...t2AdditionalColumns]
      const valuesResponse = {
        values: [
          'Title Header',
          gsheetHeaders,
          arrayOfLength(gsheetHeaders.length, 'row1'),
          arrayOfLength(gsheetHeaders.length, 'row2'),
        ]
      }
      it('separates headers and data rows', () => {
        const results = formatPesticideTable('2', valuesResponse, t2TableHeadersToColumns, t2AdditionalColumns)
        expect(results).toHaveProperty('headers')
        expect(results).toHaveProperty('data')
  
        // headers, rows, and columns should have correct dimensions
        const t2DatabaseHeaders = [
          ...Object.values(t2TableHeadersToColumns),
          ...t2AdditionalColumns,
        ]
        expectEqualIgnoringOrder(results.headers, t2DatabaseHeaders)
        expect(results.data).toHaveLength(2)
        expect(results.data[0]).toHaveLength(t2DatabaseHeaders.length)
        expect(false)
      })
    })
  
    describe('formatSimpleTable()', () => {
      dbHeaders = Object.keys(pesticideTypeTableHeadersToColumns)
      const valuesResponse = {
        values: [
          'Title Header',
          dbHeaders,
          arrayOfLength(dbHeaders.length, 'row1'),
          arrayOfLength(dbHeaders.length, 'row2'),
        ]
      }
      it('separates headers and data rows', () => {
        const results = formatSimpleTable(valuesResponse, pesticideTypeTableHeadersToColumns)
        expect(results).toHaveProperty('headers')
        expect(results).toHaveProperty('data')
  
        // headers, rows, and columns should have correct dimensions
        const databaseHeaders = Object.values(pesticideTypeTableHeadersToColumns)
        expectEqualIgnoringOrder(results.headers, databaseHeaders)
        expect(results.data).toHaveLength(2)
        expect(results.data[0]).toHaveLength(databaseHeaders.length)
        expect(false)
      })
    })
  })

  describe('utils', () => {
    describe('trimNotes()', () => {
      it('normalizes row lengths for a 2d array', () => {
        const headers = ['h1', 'h2', 'h3']
        const data = [
          [1, 1, 1],
          [1, 1, 1, 1],
          [1, 1, 1],
          [1, 1],
          [1, , 1],
        ]
        const results = trimNotes(headers, data)
        expect(results).toEqual([
          [1, 1, 1],
          [1, 1, 1],
          [1, 1, 1],
          [1, 1],
          [1, , 1],
        ])
      })
    })

    describe('addAdditionalColumns()', () => {
      it('adds a new column to header and row data', () => {
        const headers = ['h1', 'h2', 'h3']
        const data = [
          [1, 1, 1],
          [1, 1, 1],
          [1, 1],
          [1, , 1],
        ]
        const newHeaders = ['new header 1', 'fresh_header-b']
        ;([updatedHeaders, updatedData] = addAdditionalColumns(headers, data, newHeaders))
        expect(updatedHeaders).toEqual([
          ...headers,
          ...newHeaders
        ])
        expect(updatedData.every(row => row.length === headers.length + newHeaders.length))
      })
    })

    describe('handleBooleans()', () => {
      it('coerces fields specified in config.booleaFields to boolean values', () => {
        const booleanFieldHeaders = booleanFields.slice(0, 2)
        const notBooleanFieldHeaders = ['asdjflkasjf', 'weiruoefv']
        const headers = [
          ...booleanFieldHeaders,
          ...notBooleanFieldHeaders
        ]
        const data = Array.from({ length: 5 }, 
          () => Array.from(headers,
            () => 1
          )
        )
        const results = handleBooleans(headers, data)
        results.forEach(row => {
          const shouldBeBooleans = row.slice(0, 2)
          const shouldNotBeBooleans = row.slice(-2)
          expect(shouldBeBooleans.every(item => typeof item === 'boolean'))
          expect(shouldNotBeBooleans.every(item => typeof item === 'number'))
        })

      })
    })

    describe('setRiskTable()', () => {
      it('sets the risk_table field for each row', () => {
        const headers = ['a', 'b', 'risk_table', 'c']
        const data = [
          [1, true, undefined, 'hi'],
          [1, true, 'something', 'hi'],
          [1, true, 1, 'hi'],
        ]
        const results = setRiskTable(headers, data, '1')
        expect(results).toEqual([
          [1, true, '1', 'hi'],
          [1, true, '1', 'hi'],
          [1, true, '1', 'hi'],
        ])
      })
    })

    describe('splitData()', () => {
      const headers = ['a', 'b', 'c', 'd', 'e']
      const columnsToTable = {
        a: 'table1',
        b: 'table2',
        c: 'table1',
        d: 'table1',
        e: 'table3',
      }
      const data = [
        [1, 2, 1, 1, 3],
        [1, 2, 1, 1, 3],
      ]
      const results = splitData(headers, data, columnsToTable)
      results.forEach(({ table, headers, data }) => {
        switch(table) {
          case 'table1':
            expect(headers).toEqual(['a', 'c', 'd'])
            expect(data).toEqual([
              [1, 1, 1],
              [1, 1, 1],
            ])
            break;
          case 'table2':
            expect(headers).toEqual(['b'])
            expect(data).toEqual([[2], [2]])
            break;
          case 'table3':
            expect(headers).toEqual(['b'])
            expect(data).toEqual([[3], [3]])
            break;
          default:
        }
      })
    })
    
    describe('formatType()', () => {
      it('reformats JS values to postgreSQL equivalents', () => {
        const js_psql = [
          ['string', 'string'],
          [true, true],
          [false, false],
          [0, 0],
          [1, 1],
          [-5, -5],
          [undefined, undefined],
        ]
        js_psql.forEach(([jsValue, psqlValue]) => {
          expect(formatType(jsValue)).toStrictEqual(psqlValue)
        })
      })
    })

    describe('buildBulkInsertQuery()', () => {
      const table = 'dogs'
      const headers = ['name', 'age', 'goodboi', 'woof']
      const data = [
        ['milk', 3, true, undefined],
        ['max', 4, true, undefined]
      ]
      const [queryString, values] = buildBulkInsertQuery(table, headers, data)
      expect(queryString).toStrictEqual(`
INSERT INTO dogs 
(name, age, goodboi, woof)
VALUES 
($1, $2, $3, $4),
($5, $6, $7, $8);`)
      expect(values).toEqual(['milk', 3, true, undefined, 'max', 4, true, undefined])
    })

    describe('buildRowInsertQuery()', () => {
      const queryData = {
        table: 'dogs',
        headers: ['name', 'age', 'goodboi', 'woof'],
        data: ['milk', 3, true, undefined],
      }
      const [queryString, values] = buildRowInsertQuery(queryData)
      expect(queryString).toStrictEqual(`
INSERT INTO dogs 
(name, age, goodboi, woof)
VALUES 
($1, $2, $3, $4)
RETURNING *;
`)
      expect(values).toEqual(['milk', 3, true, undefined])
    })
  })
})