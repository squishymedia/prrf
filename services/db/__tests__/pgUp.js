const pgSetup = require('@databases/pg-test/jest/globalSetup')
const getPool = require('./test_pool')
const pgTeardown = require('@databases/pg-test/jest/globalTeardown')

async function test() {
  await pgSetup();
  console.log('setup')
  const pool = await getPool()
  console.log('got pool')
}
test()