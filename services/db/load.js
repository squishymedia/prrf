const RUN = process.argv[2] === 'run'
const TEST = process.argv[2] === 'test'

const pool = TEST ? require('./test-pool.js') : require('./pool.js')
console.log('Loading database ' + `${pool.options.database}`)
let CREATE_DB_TABLES = require('fs')
  .readFileSync(`${__dirname}/create_db.sql`, 'utf8')
const { 
  migratePesticideType, migrateChemClass, migrateMode, migrateTarget, migrateNaturalEnemy,
  migrateTable1, migrateTable2, migrateTable3a, migrateTable3b,
  migratePesticide_Target, migrateTarget_NaturalEnemy, migrateNaturalEnemy_Pesticide,
} = require('./migrate')

module.exports = async (pool, testEnv=false) => {
  pool.on('error', error => {
    console.error('error event on pool (%s)', error);
  });
  try {
    if (testEnv) {
      CREATE_DB_TABLES = CREATE_DB_TABLES.replace(/(OWNER TO )(postgres)(;)/g, '$1\"test-user\"$3')
    }
    console.log('pre create_db_tables')
    // await pool.query(CREATE_DB_TABLES)
    const removedComments = CREATE_DB_TABLES
      .split('\n')
      .filter(line => line && !line.startsWith('--'))
      .join('\n')
    const splitQuery = removedComments.split(';').map(line => `${line};`)
    for (let i = 0; i < splitQuery.length; i++) {
      await pool.query(splitQuery[i])
    }
    console.log('post create_db_tables')
    await Promise.all([
      (async () => {
        console.log('Migrating type...')
        await migratePesticideType(pool)
        console.log('Migration of type is complete.')
      })(),
      (async () => {
        console.log('Migrating chemical_classification...')
        await migrateChemClass(pool)
        console.log('Migration of chemical_classification is complete.')
      })(),
      (async () => {
        console.log('Migrating mode_of_action...')
        await migrateMode(pool)
        console.log('Migration of mode_of_action is complete.')
      })(),
      (async () => {
        console.log('Migrating target_pest...')
        await migrateTarget(pool)
        console.log('Migration of target_pest is complete.')
      })(),
      (async () => {
        console.log('Migrating natural_enemy...')
        await migrateNaturalEnemy(pool)
        console.log('Migration of natural_enemy is complete.')
      })(),
    ])

    await Promise.all([
      (async () => {
        console.log('Migrating table1...')
        await migrateTable1(pool)
        console.log('Migration of table1 is complete.')
      })(),
      (async () => {
        console.log('Migrating table2...')
        await migrateTable2(pool)
        console.log('Migration of table2 is complete.')
      })(),
      (async () => {
        console.log('Migrating table3a...')
        await migrateTable3a(pool)
        console.log('Migration of table3a is complete.')
      })(),
      (async () => {
        console.log('Migrating table3b...')
        await migrateTable3b(pool)
        console.log('Migration of table3b is complete.')
      })(),
    ])
    await Promise.all([
      (async () => {
        console.log('Migrating many_pesticide_has_many_target_pest...')
        await migratePesticide_Target(pool)
        console.log('Migration of many_pesticide_has_many_target_pest is complete.')
      })(),
      (async () => {
        console.log('Migrating many_target_pest_has_many_natural_enemy...')
        await migrateTarget_NaturalEnemy(pool)
        console.log('Migration of many_target_pest_has_many_natural_enemy is complete.')
      })(),
      (async () => {
        console.log('Migrating many_natural_enemy_has_many_pesticide...')
        await migrateNaturalEnemy_Pesticide(pool)
        console.log('Migration of many_natural_enemy_has_many_pesticide is complete.')
      })(),
    ])

    console.log('Migration complete.')
  } finally {
    pool.end()
  }
}

if (RUN) module.exports(pool).catch(err => console.log(err.stack))
else if (TEST) module.exports(pool, true).catch(err => console.log(err.stack))