const fs = require('fs')
const readline = require('readline')
const { google } = require('googleapis')
const { tableSheetID } = require('./config.js')

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = `${__dirname}/gheets-token.json`

const DEFAULT_SHEET = 'Sheet1'

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0])

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback)
    oAuth2Client.setCredentials(JSON.parse(token))
    callback(oAuth2Client)
  })
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  })
  console.log('\nYou must log in with an account that has access to the data tables.\n')
  console.log('Authorize this app by visiting this url:', authUrl)
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  })
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close()
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error while trying to retrieve access token', err)
      oAuth2Client.setCredentials(token)
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err)
        console.log(`\nToken stored to ${TOKEN_PATH}\n`)
      })
      callback(oAuth2Client)
    })
  })
}

/**
 * Returns the authorized v4 google sheets api.
 */
const withAuth = () => 
  new Promise((resolve, reject) => {
    // Load client secrets from a local file.
    fs.readFile(`${__dirname}/gsheets-credentials.json`, (err, content) => {
      if (err) reject(err)
      // Authorize a client with credentials, then call the Google Sheets API.
      authorize(JSON.parse(content), auth => {
        const gSheets = google.sheets({version: 'v4', auth})
        resolve(gSheets)
      })
    })
  })

/**
 * Get a specific sheet from a google spreadsheet.
 *   spreadsheets != sheets; spreadsheets are the document, sheets are the pages/tabs in the document
 * @param {string} spreadsheetId The desired spreadsheet's id (in url).
 * @param {string} sheetTitle The title of the desired sheet (defaults to "Sheet1").
 */
const getSheet = async (spreadsheetId, sheetTitle = DEFAULT_SHEET) => {
  const gSheets = await withAuth()
  return new Promise((resolve, reject) => {
    gSheets.spreadsheets.get({
      spreadsheetId,
      includeGridData: true,
    }, (err, res) => {
      if (err) reject(err)
      const spreadsheet = res.data
      spreadsheet.sheets.forEach(sheet => console.log(sheet))
      const sheet = spreadsheet.sheets.find(sheet => sheet.properties.title === sheetTitle)
      resolve(sheet)
    })
  })
}

/**
 * Gets the number of formatted rows and columns.
 * @param {string} spreadsheetId The desired spreadsheet's id (in url).
 * @param {string} sheetTitle The title of the desired sheet (defaults to "Sheet1").
 */
const getSheetDimensions = async (spreadsheetId, sheetTitle) => {
  const sheet = await getSheet(spreadsheetId, sheetTitle)
  const rows = sheet.data[0].rowData.length
  const columns = sheet.data[0].rowData[162].values.length
  return { rows, columns }
}

const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const colLetterMap = [
  ...letters.split(''),
  ...letters.split('').map(c => c+c)
]
// index 1
const getColumnLetter = n => colLetterMap[n+1]

const toA1Notation = (endRow, endColumn, sheetTitle=DEFAULT_SHEET, startRow=0, startColumn=0) => {
  startRow = startRow+1
  endRow = endRow+1
  startColumn = getColumnLetter(startColumn)
  endColumn = getColumnLetter(endColumn)
  return `${sheetTitle}!${startColumn}${startRow}:${endColumn}${endRow}`
}

const getValues = async (range=DEFAULT_SHEET) => {
  const gSheets = await withAuth()
  return new Promise((resolve, reject) => {
    gSheets.spreadsheets.values.get({
      spreadsheetId: tableSheetID,
      range,
    }, (err, res) => {
      if (err) reject(err)
      resolve(res.data)
    })
  })
}

module.exports = {
  getSheet,
  getSheetDimensions,
  getValues,
  toA1Notation,
}