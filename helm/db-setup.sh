#!/bin/bash
# There are two variants of this script - changes should be applied to both
# See bin/db-setup.sh for the docker-compose version
set -e

# create users and db
psql -v ON_ERROR_STOP=1 --username "${PGUSER}" --dbname "${POSTGRES_DB}" <<-EOSQL
    GRANT ALL PRIVILEGES ON DATABASE ${POSTGRES_DB} TO ${PG_APP_USER};
    CREATE EXTENSION IF NOT EXISTS pgcrypto;
    CREATE SCHEMA IF NOT EXISTS hdb_catalog;
    CREATE SCHEMA IF NOT EXISTS hdb_views;
    ALTER SCHEMA hdb_catalog OWNER TO ${PG_APP_USER};
    ALTER SCHEMA hdb_views OWNER TO ${PG_APP_USER};
    GRANT SELECT ON ALL TABLES IN SCHEMA information_schema TO ${PG_APP_USER};
    GRANT SELECT ON ALL TABLES IN SCHEMA pg_catalog TO ${PG_APP_USER};
    GRANT USAGE ON SCHEMA public TO ${PG_APP_USER};
    GRANT ALL ON ALL TABLES IN SCHEMA public TO ${PG_APP_USER};
    GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO ${PG_APP_USER};
    GRANT ALL ON ALL FUNCTIONS IN SCHEMA public TO ${PG_APP_USER};
EOSQL

# load data
psql --username "${PGUSER}" "${POSTGRES_DB}" < /tmp/backups/prrf_07-20-2021_2.bak
