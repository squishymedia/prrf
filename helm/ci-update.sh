#!/bin/bash -x
export RELEASE_NAME="prrf-${CI_ENVIRONMENT_SLUG}"
export IMAGE=$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
export DEPLOYS=$(helm ls | grep $RELEASE_NAME | wc -l)
if [ ${DEPLOYS}  -eq 0 ]; then
  helm install --set postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD} --set postgresql.postgresqlPostgresPassword=${POSTGRESQL_POSTGRES_PASSWORD} --set image=${IMAGE} --set domain=$CI_ENVIRONMENT_SLUG.prrf.sqm.io ${RELEASE_NAME} .
  kubectl wait --for=condition=ready --timeout=120s pod ${RELEASE_NAME}-postgresql-0
  kubectl cp db-setup.sh ${RELEASE_NAME}-postgresql-0:/tmp/db-setup.sh
  kubectl cp ../services/db/backups/ ${RELEASE_NAME}-postgresql-0:/tmp/backups
  kubectl exec ${RELEASE_NAME}-postgresql-0 -- env POSTGRES_DB=prrf PG_APP_USER=prrf PG_APP_PASSWORD=${POSTGRESQL_PASSWORD} PGUSER=postgres PGPASSWORD=${POSTGRESQL_POSTGRES_PASSWORD} /tmp/db-setup.sh
  # Load hasura metadata
  kubectl wait --for=condition=available --timeout=120s deployment ${RELEASE_NAME}-hasura
  kubectl exec ${RELEASE_NAME}-postgresql-0 -- curl -d'{"type":"replace_metadata", "args":'$(cat /tmp/backups/hasura_metadata.json)'}' http://${RELEASE_NAME}-hasura/v1/query
else
  helm upgrade --set postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD} --set postgresql.postgresqlPostgresPassword=${POSTGRESQL_POSTGRES_PASSWORD} --set image=$IMAGE ${RELEASE_NAME} --set domain=$CI_ENVIRONMENT_SLUG.prrf.sqm.io . 
fi
