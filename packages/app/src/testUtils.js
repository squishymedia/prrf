export function removeClassNamesFromTree(elementList) {
  elementList.forEach(element => {
    if (element.props) {
      delete element.props.className
      delete element.props.dangerouslySetInnerHTML
    }
    if (element.children && element.children.length)
      removeClassNamesFromTree(element.children)
  })
}
