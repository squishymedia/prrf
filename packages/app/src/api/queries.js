export const buildSearchQueryString = searchCtx =>
  searchCtx.filters.text || searchCtx.filters.riskMitigation !== ""
    ? searchTextQuery(searchCtx)
    : null

const PESTICIDE_SEARCH_RESULT_FIELDS = `
  cas_number
  id
  name
  risk_table
  risk_mitigation {
    aquatic
    better_personal_protection
    bystander
    pollinator
    wildlife
  }
  pesticide_type {
    type
  }
  mode_of_action {
    code
  }
`

export const buildTextlessSearchParams = (
  { sortOrder, pagination, filters },
  withPagination = true
) => {
  const params = [
    withPagination && `limit: ${pagination.itemsPerPage}`,
    withPagination && `offset: ${pagination.offset}`,
    `order_by: {${sortOrder.orderBy}}`,
    filters.riskMitigation !== "" &&
      `where: {risk_mitigation: {${filters.riskMitigation}: {_eq: true}}}`,
  ]
    .filter(exists => exists) // remove any filters that arent active or set
    .join(", ")
  return ` (${params})` || ""
}

// export const textlessSearchQuery = searchCtx => {
//   const parameters = buildTextlessSearchParams(searchCtx)
//   return `
//     query SearchResults{
//       pesticide${parameters} {
//         ${PESTICIDE_SEARCH_RESULT_FIELDS}
//       }
//     }
//   `
// }

export const buildSearchTextParams = (
  { filters, sortOrder, pagination },
  withPagination = true
) =>
  "args: {search: $text}, " +
  [
    withPagination && `limit: ${pagination.itemsPerPage}`,
    withPagination && `offset: ${pagination.offset}`,
    `order_by: {${sortOrder.orderBy}}`,
    filters.riskMitigation !== "" &&
      `where: {risk_mitigation: {${filters.riskMitigation}: {_eq: true}}}`,
  ]
    .filter(exists => exists) // remove any filters that arent active or set
    .join(", ")

export const searchTextQuery = searchCtx => {
  const parameters = buildSearchTextParams(searchCtx)
  return `
    query SearchResults {
      search_text(${parameters}) {
        ${PESTICIDE_SEARCH_RESULT_FIELDS}
      }
    }
  `
}

export const buildSearchByIdQuery = id => ({
  query: `
    query PesticideDetail ($id: Int!) {
      pesticide(where: { id: { _eq: $id } }) {
        cas_number
        id
        name
        hazard {
          ghs_cancer_1ab
          ghs_mutagenic_1ab
          ghs_reproductive_1ab
          montreal_protocol
          rotterdam_convention
          severe_effects
          stockholm_convention
          who_1a
          who_1b
        }
        chemical_classification {
          class
          sub_class
        }
        mode_of_action {
          code
          term
        }
        pesticide_type {
          type
        }
        obsolete
        risk_mitigation {
          aquatic
          better_personal_protection
          bystander
          id
          id_pesticide
          pollinator
          wildlife
        }
        risk_table
      }
    }
  `,
  variables: {
    id,
  },
})

export const buildPesticideCountQueryString = searchCtx => {
  if ((searchCtx.filters.text || searchCtx.filters.riskMitigation) !== "") {
    const params = buildSearchTextParams(searchCtx, false)
    return {
      query: `
        query ($text: String!) {
          search_text_aggregate(${params}) {
            aggregate {
              count
            }
          }
        }
      `,
      variables: {
        text: searchCtx.filters.text,
      },
    }
  } else {
    const params = buildTextlessSearchParams(searchCtx, false)
    return {
      query: `
        query {
          pesticide_aggregate${params} {
            aggregate {
              count
            }
          }
        }
      `,
    }
  }
}

export const buildSubstringSearchQuery = ({
  filters: { text, riskMitigation },
  sortOrder: { orderBy },
}) => {
  const conditions = [
    text && `name: {_ilike: $text}`,
    riskMitigation && `risk_mitigation: {${riskMitigation}: {_eq: true}}`,
  ].filter(exist => exist)

  return {
    query: `
      query ${text ? "($text: String!)" : ""} {
        pesticide(
          ${conditions.length ? `where: {${conditions.join(",")}},` : ""}
          order_by: {${orderBy}}
        ) {
          ${PESTICIDE_SEARCH_RESULT_FIELDS}
        }
      }
    `,
    variables: {
      text,
    },
  }
}
