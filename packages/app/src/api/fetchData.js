import { GraphQLClient } from "graphql-request"

const client = new GraphQLClient(
  process.env.GATSBY_PRRF_API_URL || "/v1/graphql",
  {
    headers: { "Content-Type": "application/json" },
  }
)

const fetchData = async ({ query, variables = {} }) => {
  try {
    const res = await client.request(query, variables)
    return res
  } catch (err) {
    throw Error(err)
  }
}

export default fetchData
