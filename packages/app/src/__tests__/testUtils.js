import { removeClassNamesFromTree } from '../testUtils'

describe('removeClassNamesFromTree', () => {
  it('recursively removes className and dangerouslySetInnerHTML prop from component tree', () => {
    const componentTree = [
      {
        type: "div",
        props: {
          className: "qwerty",
          dangerouslySetInnerHTML: "devious code",
          href: "google.com",
        },
        children: [
          {
            type: "div",
            props: {
              className: "qwerty2",
              dangerouslySetInnerHTML: "devious code2",
              src: "google2.com",
            },
            children: null,
          },
        ],
      },
    ]
    removeClassNamesFromTree(componentTree)

    expect(componentTree).toEqual([
      {
        type: "div",
        props: {
          href: "google.com",
        },
        children: [
          {
            type: "div",
            props: {
              src: "google2.com",
            },
            children: null,
          },
        ],
      },
    ])
  })
})
