import { capitalized } from '../utils'
import assert from 'assert'

describe('capitalized', () => {
  it('capitalizes words', () => {
    const inputsAndOutputs = [
      ['solo', 'Solo'],
      ['mode of action', 'Mode Of Action'],
      ['two\nlines', 'Two\nlines'],
      ['tab\tseparated', 'Tab\tseparated'],
    ]
    inputsAndOutputs.forEach(([ input, output ]) => {
      assert.strictEqual(capitalized(input), output)
    })
  })
  it('capitalizes words unless specified as ignored', () => {
    const inputsAndOutputs = [
      ['solo', ['solo'], 'solo'],
      ['mode of action', ['of'], 'Mode of Action'],
      ['mode of action', ['of', 'action'], 'Mode of action'],
      ['two\nlines', undefined, 'Two\nlines'],
      ['tab\tseparated', undefined, 'Tab\tseparated'],
    ]
    inputsAndOutputs.forEach(([ input, ignored, output ]) => {
      assert.strictEqual(capitalized(input, ignored), output)
    })
  })
  it('does not affect non-strings', () => {
    const inputsAndOutputs = [
      [30, ['solo'], 30],
      [undefined, ['of'], undefined],
      [{ object: true, string: 'nope' }, ['of', 'action'], { object: true, string: 'nope' }],
      [['array'], undefined, ['array']],
    ]
    inputsAndOutputs.forEach(([ input, ignored, output ]) => {
      assert.deepStrictEqual(capitalized(input, ignored), output)
    })  
  })
})