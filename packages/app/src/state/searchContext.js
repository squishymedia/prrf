import React from "react"

export const filters = {
  riskMitigation: '',
  text: '',
}

export const sortOrder = {
    orderBy: "name: asc"
}

export const pagination = {
  itemsPerPage: 50,
  offset: 0,
  totalItems: 0,
}

export const isInitialSearch = true

const SearchContext = React.createContext({
  filters,
  sortOrder,
  pagination,
  isInitialSearch
})

export { SearchContext }
