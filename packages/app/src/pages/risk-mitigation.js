import React from "react"
import { GridContainer, Grid } from "@trussworks/react-uswds"

import Layout from "../components/layout"
import SEO from "../components/seo"

const RiskMitigation = () => {
  return (
    <Layout>
      <SEO title="Risk Mitigation" />
      <GridContainer className="padding-y-3 ">
        <Grid row className="flex-justify-center">
          <Grid col={8} className="border-bottom-1 border-secondary-light ">
            <h1>
              Risk mitigation for exceptional uses of HHPs listed in Table 1{" "}
            </h1>
          </Grid>
        </Grid>
        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h2>Reproductive toxins</h2>
            <p className="line-height-mono-4">
              At least eight HHPs with reproductive toxicity are in widespread
              use:{" "}
              <b>
                borax, boric acid, carbendazim, epoxiconazole, fluazifop-butyl,
                glufosinate-ammonium, quizalfop-p-tefuryl, and tridemorph
              </b>
              . Replacing the use of these pesticides with lower risk chemicals,
              or non-chemical practices, is an important priority for risk
              management. Where use is considered essential, or during
              phase-out, the following risk management guidelines should be
              implemented.
            </p>
            <ol className="usa-list">
              <li>
                Include the use of essential HHPs as part of a rotation with
                less toxic pesticides.
              </li>
              <li>
                Women of reproductive age (15-50 years) should not apply
                pesticides that are classified as reproductive toxins.
              </li>
              <li>
                Workers applying reproductive toxins should wear the most
                protective clothing that is available, including
                chemically-resistant gloves, boots and socks, long pants and
                long-sleeved shirt, overalls that cover trousers and shirt,
                respirators and face masks, and should exercise high levels of
                caution when handling pesticide containers and sprayers.
              </li>
              <li>
                Restricted entry intervals should be implemented for all persons
                entering the field after application. These should be at least
                48h, or as indicated on the pesticide label, whichever is
                greater.
              </li>
              <li>
                Potentially affected persons or communities should be identified
                and alerted in advance of application, flags should be used to
                identify treated fields, and access to treated fields should be
                prevented.
              </li>
              <li>
                Non-crop <b>vegetative barriers</b> and/or{" "}
                <b>non-application zones</b> should be maintained between
                treated fields and areas of human activity.
              </li>
            </ol>
          </Grid>
        </Grid>
        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h2>Nematicides</h2>
            <p className="line-height-mono-4">
              Most nematicides are highly toxic, and many are HHPs. There are
              less toxic alternatives entering the marketplace, and there are
              many non-pesticide options available, particularly prior to
              planting. However, a risk mitigation program should be implemented
              while alternatives are sought. The list below, developed by the
              SAN under international review, provides an example risk
              mitigation program for five nematicides that meet the criteria for
              HHP designation:{" "}
              <b>cadusafos, ethoprop, fenamiphos, oxamyl and terbufos</b>:
            </p>
            <ol className="usa-list">
              <li>
                Use lower toxicity nematicides as part of the rotation of
                nematicides for resistance management.
              </li>
              <li>
                Use application methods that place the product precisely within
                the plant root zone, or use tree injection.
              </li>
              <li>
                Workers applying reproductive toxins should wear the most
                protective clothing that is available, including
                chemically-resistant gloves, boots and socks, long pants and
                long-sleeved shirt, overalls that cover trousers and shirt,
                respirators and face masks, and should exercise high levels of
                caution when handling pesticide containers and sprayers.
              </li>
              <li>
                Avoid uncovered application of granules, particularly close to
                aquatic areas.
              </li>
              <li>
                Limit daily maximum application time for applicators to eight
                hours, in two shifts, with bathing to wash off residues.
              </li>
              <li>Make applications during the coolest hours of the day.</li>
              <li>
                Monitor applicator health annually for kidney and liver
                function.
              </li>
            </ol>
          </Grid>
        </Grid>

        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h2>Rodenticides</h2>
            <p className="line-height-mono-4">
              Many rodenticides are, by definition, mammalian toxins, and many
              are HHPs. The following risk management guidelines are important
              to limit risks when using{" "}
              <b>
                brodifacoum, bromadiolone, bromethalin, chlorophacinone,
                difethialone, diphacinone, strychnine, warfarin and zinc
                phosphide
              </b>
              :
            </p>
            <ol className="usa-list">
              <li>
                Food sources attracting rodents and debris should be eliminated.
              </li>
              <li>
                Signs of rodent activity (droppings, tracks, gnaw marks,
                burrows) should be monitored and the results recorded. Traps
                should be inspected daily, and bait stations and installations
                weekly.
              </li>
              <li>
                Rodenticide-baited traps should only be used inside buildings
                if, and only if, rodent monitoring demonstrates that
                mechanical-only control methods are not effective.
              </li>
              <li>
                Use of rodenticides should be restricted mainly to the
                protection of storage places for harvested crops, and crop
                packing stations and handling facilities.
              </li>
              <li>
                Routine field applications should not be undertaken: the need
                for them should be determined by scouting, and applications
                should be localized and their effects monitored.
              </li>
              <li>
                Only formulated rodenticide-baited traps classified as
                moderately toxic (blue label in some countries) or slightly
                toxic (green label in some countries) should be used.
              </li>
              <li>
                Bait stations should be tamper- resistant, anchored, and
                constructed in such a manner and size as to permit entrance only
                by rodents.
              </li>
              <li>
                Rodent carcasses should be handled with gloves and buried in
                locations that do not pose risks to human or wildlife health or
                water contamination.
              </li>
              <li>
                Bait stations should be removed and the amount of stations
                diminished when there are no longer signs of rodent feeding or,
                if there is evidence of use by non- target wildlife.
              </li>
            </ol>
          </Grid>
        </Grid>

        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h2>Pesticides classified as HHPs because of pollinator impacts</h2>
            <p className="line-height-mono-4">
              The three{" "}
              <b>
                neonicotinoids clothianodin, imidacloprid, thiamethoxam , and
                the phenylpyrazole fipronil
              </b>{" "}
              were classified as HHPs because of evidence for their adverse
              impacts on pollinator populations. Open field uses of these
              pesticides should be minimized and phased out, and treatment of
              perennial crops should be avoided because of their persistence,
              and accumulation in plant tissues. If less toxic and efficacious
              pesticides are not available, and if any of these insecticides are
              used in annual crops during a phase-out period, mitigation
              measures that limit <em>RISK TO POLLINATORS</em> should be used.
              Farms should apply substances listed in Table 2 as having{" "}
              <em>RISK TO POLLINATORS</em> only if:
            </p>
            <ul className="usa-list">
              <li>
                Less toxic, efficacious pesticides are <b>not</b> available
              </li>
              <li>
                Exposure of natural ecosystems to pesticides is minimized by
                establishing <b>non-application zones</b>, or functional{" "}
                <b>vegetative barriers</b>
              </li>
              <li>
                Contact of pollinators with these substances is further reduced
                <ul>
                  <li>
                    Substances are not applied to flowering weeds or flowering
                    weeds are removed
                  </li>
                  <li>
                    Substances are not applied while the crop is in its peak
                    flowering period
                  </li>
                </ul>
              </li>
            </ul>
            <h3>Specifications for risk mitigation methods</h3>
            <p className="line-height-mono-4">
              When applying synthetic pesticides and fertilizers, spray drift
              and run-off to natural ecosystems and zones of human activity, can
              be prevented by implementing:
              <ol className="usa-list">
                <li>
                  <b>Non-application zones</b> between treated crops and these
                  areas
                  <ol>
                    <li>
                      5 meters (5.5 yards) wide for application by mechanical,
                      hand-assisted, and targeted application methods, such as:
                      knapsack sprayers, banding, baiting, specific granule
                      placement, soil or plant injection, seed treatments and
                      weed wiping
                    </li>
                    <li>
                      10 meters (11 yards) wide for application by broadcast or
                      pressurized spray application methods, such as: motorized
                      sprayers, or spray booms, air blast sprayers, foggers
                      (including ultra-low-volume (ULV) fogging machines),
                      depending on the equipment’s specifications
                    </li>
                  </ol>
                </li>
                <li>
                  Functional <b>vegetative barriers</b> that are
                  <ol>
                    <li>
                      As high as the crop height or the height of the
                      equipment’s application nozzles over the ground, whichever
                      is higher
                    </li>
                    <li>
                      Composed of plants that maintain their foliage all year,
                      but which are permeable to airflow, allowing the barrier
                      to capture pesticide drops
                    </li>
                    <li>Composed of non-invasive species</li>
                  </ol>
                </li>
              </ol>
            </p>
          </Grid>
        </Grid>

        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h3>Adapted from: </h3>
            <small>
              <b>
                Jepson, P.C., Murray, K., Bach, O., Bonilla, M.A., Neumeister,
                L.
              </b>{" "}
              Selection of pesticides to reduce human and environmental health
              risks: a global guideline and minimum pesticides list. Lancet
              Planetary Health 2020; <b>4</b>: e56-63.
            </small>
            <div>
              <small>
                DOI:{" "}
                <a href="https://doi.org/10.1016/S2542-5196(19)30266-9">
                  https://doi.org/10.1016/S2542-5196(19)30266-9
                </a>
              </small>
            </div>
          </Grid>
        </Grid>
      </GridContainer>
    </Layout>
  )
}

export default RiskMitigation
