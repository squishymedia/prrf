import React from "react"
import { useFetchData, useSearchParams } from "../hooks"
import { buildSearchByIdQuery } from "../api/queries"
import PesticideDetail from "../components/pesticideDetail"
import Loader from "../components/loader"
import SEO from "../components/seo"
import Layout from "../components/layout"

function Detail({ location }) {
  const { id } = useSearchParams(location.search)
  const { loading, data } = useFetchData(buildSearchByIdQuery(id))

  const pesticide = data.pesticide?.[0]

  return loading ? (
    <Loader />
  ) : (
    <Layout>
      <SEO title={pesticide.name} />
      <PesticideDetail pesticide={pesticide} />
    </Layout>
  )
}

export default Detail
