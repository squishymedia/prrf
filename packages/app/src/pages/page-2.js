import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { useFetchData } from "../hooks"
import { buildSearchByIdQuery } from "../api/queries"

const SecondPage = () => {
  const { loading, data } = useFetchData(buildSearchByIdQuery("42"))
  return (
    <Layout>
      <SEO title="Page two" />
      {loading ? <h1>LOADING</h1> : <pre>{JSON.stringify(data, null, 2)}</pre>}
      <Link to="/">Go back to the homepage</Link>
    </Layout>
  )
}

export default SecondPage
