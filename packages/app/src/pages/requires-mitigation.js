import React from "react"
import { GridContainer, Grid } from "@trussworks/react-uswds"

import Layout from "../components/layout"
import SEO from "../components/seo"

const RequiresMitigation = () => {
  return (
    <Layout>
      <SEO title="Requires Mitigation" />
      <GridContainer className="padding-y-3 ">
        <Grid row className="flex-justify-center">
          <Grid col={8} className="border-bottom-1 border-accent-warm-light ">
            <h1>Pesticides Requiring Risk Mitigation </h1>
          </Grid>
        </Grid>
        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <p className="line-height-mono-4">
              Table 2 lists pesticides that require specific risks to be
              mitigated if impacts to human health or the environment are to be
              limited. The risk analysis is based a state-of-the-science risk
              assessment tool that identifies pesticides with moderate to high
              (10% or greater) risks to aquatic life, terrestrial wildlife,
              and/or bystander inhalation, and a 15% (or greater) risk of honey
              bee hive loss. These risks require mitigation to reduce the
              likelihood of unacceptable impacts (see Jepson et al., 2014).{" "}
            </p>
            <h2>The risk categories are:</h2>
            <ol className="usa-list">
              <li>
                <b>RISK TO AQUATIC LIFE:</b> Pesticides qualified for this risk
                category if one or more aquatic risk models (aquatic algae,
                aquatic invertebrates, or fish chronic risk) exhibited &gt; 10%
                risk at a typical application rate
              </li>
              <li>
                <b>RISK TO TERRESTRIAL WILDLIFE:</b>Pesticides qualified for
                this risk category if one or more terrestrial risk models (avian
                reproductive, avian acute, or small mammal risk) exhibited &gt;
                10% risk at a typical application rate
              </li>
              <li>
                <b>RISK TO POLLINATORS:</b> Pesticides were selected here based
                on a widely-used hazard quotient (HQ) resulting from pesticide
                application rate in g a.i./ha (a.i. = active ingredient), and
                contact LD50 for the honey bee <em>(Apis mellifera)</em>
              </li>
              <li>
                <b>INHALATION RISK:</b> Inhalation risk to bystanders was
                calculated using a model for inhalation toxicity calculated on
                the basis of child exposure and susceptibility
              </li>
            </ol>
            <p className="line-height-mono-4">
              This summary applies primarily to pesticides listed on Table 2,
              although it can also be used to improve risk management for HHPs
              in the phase out period.
            </p>
            <ol className="usa-list">
              <li>
                Pesticides listed in Table 2 as having{" "}
                <em>RISK TO AQUATIC LIFE</em>, or{" "}
                <em>RISK TO TERRESTRIAL WILDLIFE</em>, should only be applied
                if:{" "}
                <ol className="ol-alpha">
                  <li>
                    <b>Non- application zones</b> are used around natural
                    ecosystems and sensitive sites; or{" "}
                  </li>
                  <li>
                    <b>Vegetative barriers, or riparian and wetland buffers</b>{" "}
                    (see specifications for these methods below) are
                    established; or c. other effective mechanisms are used to
                    reduce spray drift.
                  </li>
                </ol>
              </li>
              <li>
                Farms should also establish and maintain non-crop{" "}
                <b>vegetative barriers and non- application zones</b> between
                pesticide- treated crops and areas of human activity.
              </li>
              <li>
                Farms should apply substances listed in Table 2 as having{" "}
                <em>RISK TO POLLINATORS</em> only if:
                <ol className="ol-alpha">
                  <li>
                    Less toxic, efficacious pesticides are <b>not</b> available;
                  </li>
                  <li>
                    Exposure of natural ecosystems to pesticides is minimized by
                    establishing non-application zones, or functional{" "}
                    <b>vegetative barriers</b>; and
                  </li>
                  <li>
                    Contact of pollinators with these substances is further
                    reduced:
                    <ol className="ol-roman">
                      <li>
                        Substances are not applied to flowering weeds or
                        flowering weeds are removed;
                      </li>
                      <li>
                        and ii. Substances are not applied while the crop is in
                        its peak flowering period.
                      </li>
                    </ol>
                  </li>
                </ol>
              </li>

              <li>
                Pesticides listed in Table 2 as having <em>INHALATION RISK</em>{" "}
                should only be applied if
                <ol className="ol-alpha">
                  <li>Restricted Entry Intervals (REIs) are enforced; and </li>
                  <li>
                    Respirators with an organic vapor (OV) cartridge or canister
                    with any N, R, P, or 100-series filter are used; and{" "}
                  </li>
                  <li>
                    All application sites are flagged to indicate inhalation
                    risks to bystanders.
                  </li>
                </ol>
              </li>
            </ol>
            <h2>Specifications for risk mitigation methods</h2>
            <p className="line-height-mono-4">
              When applying synthetic pesticides and fertilizers, spray drift
              and run-off to natural ecosystems and zones of human activity, can
              be prevented by implementing:
            </p>
            <ol className="usa-list">
              <li>
                <b>Non-application zones</b> between treated crops and these
                areas:
                <ol>
                  <li>
                    5 meters (5.5 yards) wide for application by mechanical,
                    hand-assisted, and targeted application methods, such as:
                    knapsack sprayers, banding, baiting, specific granule
                    placement, soil or plant injection, seed treatments and weed
                    wiping;
                  </li>
                  <li>
                    10 meters (11 yards) wide for application by broadcast or
                    pressurized spray application methods, such as: motorized
                    sprayers, or spray booms, air blast sprayers, foggers
                    (including ultra-low-volume (ULV) fogging machines),
                    depending on the equipment’s specifications;
                  </li>
                </ol>
              </li>
              <li>
                Functional vegetative barriers that are:
                <ol>
                  <li>
                    As high as the crop height or the height of the equipment’s
                    application nozzles over the ground, whichever is higher;
                  </li>
                  <li>
                    Composed of plants that maintain their foliage all year, but
                    which are permeable to airflow, allowing the barrier to
                    capture pesticide drops; and
                  </li>
                  <li>Composed of non-invasive species.</li>
                </ol>
              </li>

              <li>
                <b>Riparian and wetland buffers</b> that consist of:
              </li>
            </ol>
            <p className="line-height-mono-4">
              a. Vegetative buffers consisting of native vegetation, of the
              following minimum widths (water course width is defined as the
              width of the normal flow during the rainy season, but not during
              flood conditions):
            </p>
            <ol className="usa-list ol-roman">
              <li>
                5 meters (5.5 yards) horizontal width along both sides of water
                courses less than 5 meters (5.5 yards) wide;
              </li>
              <li>
                8 meters (8.75 yards) horizontal width along both sides of water
                courses 5-10 meters (5.5-11 yards) wide, and around springs,
                wetlands, and other water bodies;
              </li>
              <li>
                15 meters (16.4 yards) horizontal width along both sides of
                rivers wider than 10 meters (11 yards).
              </li>
            </ol>
            <p className="line-height-mono-4">
              We do not address the complex requirements for mitigating drift
              associated rotary- and fixed-wing aerial application.
            </p>
          </Grid>
        </Grid>
        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h3>Adapted from: </h3>
            <small>
              <b>
                Jepson, P.C., Murray, K., Bach, O., Bonilla, M.A., Neumeister,
                L.
              </b>{" "}
              Selection of pesticides to reduce human and environmental health
              risks: a global guideline and minimum pesticides list. Lancet
              Planetary Health 2020; <b>4</b>: e56-63.
            </small>
            <div>
              <small>
                DOI:{" "}
                <a href="https://doi.org/10.1016/S2542-5196(19)30266-9">
                  https://doi.org/10.1016/S2542-5196(19)30266-9
                </a>
              </small>
            </div>
          </Grid>
        </Grid>
        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h3>And</h3>
            <small>
              <b>
                Jepson, P.C., Guzy, M., Blaustein, K., Sow, M., Sarr, M.,
                Mineau, P., Kegley, S. (2014)
              </b>{" "}
              Measuring pesticide ecological and health risks in West African
              agriculture to establish an enabling environment for sustainable
              intensification. Philosophical Transactions of the Royal Society
              <b>B</b>.
            </small>
            <div>
              <small>
                DOI:{" "}
                <a href="http://dx.doi.org/10.1098/rstb.2013.0491">
                  http://dx.doi.org/10.1098/rstb.2013.0491
                </a>
              </small>
            </div>
          </Grid>
        </Grid>
      </GridContainer>
    </Layout>
  )
}

export default RequiresMitigation
