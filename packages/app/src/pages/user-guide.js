import React from "react"
import { GridContainer, Grid } from "@trussworks/react-uswds"

import Layout from "../components/layout"
import SEO from "../components/seo"

const UserGuid = () => {
  return (
    <Layout>
      <SEO title="User Guide" />
      <GridContainer className="padding-y-3 ">
        <Grid row className="flex-justify-center">
          <Grid col={8} className="border-bottom-1 border-accent-cool-light ">
            <h1>STEP-BY-STEP GUIDE TO USING THE PESTICIDE TABLES</h1>
          </Grid>
        </Grid>
        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <p>
              Please use the steps below to develop a plan for how to introduce
              and make use of these tables with farmers, or other IPM decision
              makers.
            </p>
            <h2>Step 1: Develop your IPM Plan</h2>
            <ul className="usa-list">
              <li>
                Consider what you can do to limit pest presence and damage
                before using pesticides, and develop a plan for adopting these
                practices.
              </li>
            </ul>
            <h2>Step 2: Develop Pesticide Lists</h2>
            <ul className="usa-list">
              <li>
                Assemble a list of all pesticides that are registered and
                available locally for your intended use.
              </li>
              <li>
                If possible, determine which of these pesticides are
                efficacious, based upon locally-relevant data.
              </li>
              <li>
                Ensure effective training in pesticide storage, transport,
                personal protection, handling, application, cleanup and
                disposal.
              </li>
              <li>
                <b>
                  Note that one may BEGIN this process with the low-risk
                  pesticideTable 3b, which lists only lower-risk pesticides that
                  may meet your pest management needs.
                </b>
              </li>
            </ul>
            <h2>Step 3: Identify and Reduce Highly Hazardous Pesticides.</h2>
            <ul className="usa-list">
              <li>
                Refer to Table 1 to determine if any of the pesticides that you
                have listed are classified as Highly Hazardous Pesticides(HHPs),
                and also determine if additional compounds have been listed as
                HHPs because of evidence for impacts locally.
              </li>
              <li>
                If no pesticide on your list is an HHP, move on to Step 4.
              </li>
              <li>
                If you have included an HHP, select a less toxic alternative
                that does not fall within this classification.
              </li>
              <li>
                If no suitable alternatives are available, and IPM alternatives
                to pesticides have been exhausted, it is essential that steps
                are taken to protect handlers, applicators, bystanders, and the
                environment. You must first follow label instructions, but
                incases where these are not available or incomplete, see the
                examples of risk management for HHPs (Table 1)
              </li>
              <li>
                Develop a plan to phase out use of any HHPs by adopting a wider
                array of IPM tactics, and by consulting experts including
                researchers, extension agents, and other farmers, about
                effective alternatives.
              </li>
            </ul>
            <h2>Step 4: Identify Pesticides Requiring Risk Mitigation.</h2>
            <ul className="usa-list">
              <li>
                Refer to Table 2 to determine if any of the pesticides that you
                have listed require risk mitigation.
              </li>
              <li>
                If no pesticide on your list requires risk mitigation, move to
                Step 5.
              </li>
              <li>
                If you are considering a pesticide that requires risk
                mitigation, consider using an alternative, lower risk pesticide
                that does not fall within this classification (Step 5).
              </li>
              <li>
                If no alternatives are available, and IPM alternatives to
                pesticides have been exhausted, it is essential that steps are
                taken to implement risk mitigation practices, and to protect
                yourself and bystanders. You must first follow label
                instructions, but in cases where these are not available or
                incomplete, we provide examples of effective risk mitigations
                for pesticides below.
              </li>
            </ul>
            <h2>Step 5: Identify and Select Lower-Risk Pesticides</h2>
            <ul className="usa-list">
              <li>
                If none of the pesticides that you are considering are HHPs or
                pesticides requiring risk mitigation according to our analysis,
                refer to Tables 3a 3b.
              </li>
              <li>
                <b>
                  IT IS ESSENTIAL THAT YOU CONSIDER LOCAL REGULATORY, HEALTH,
                  AND RESEACH INFORMATION THAT MAY FACTOR IN RISKS THAT OUR
                  ANALYSIS HAS NOT CONSIDERED, BEFORE DEVELOPING A FINAL LIST OF
                  PESTICIDES THAT MAY FIT WITHIN YOUR SYSTEM
                </b>
              </li>
              <li>
                If one or more pesticides that you are considering are not
                referred to in either Tables 1, 2, 3a or b then either, the
                risks associated with this pesticide have not yet been analyzed,
                or, the data to carry out the risk assessment are not
                available.In this case, follow label instructions, and unless
                you have clear guidance that this pesticide is of low risk,
                treat the pesticide as if all the mitigations in Step 2 are
                required, and protective clothing (PPE) is needed for handling,
                application and clean-up.
              </li>
              <li>
                If you are considering pesticides that are listed on Table 3a
                orb, then lower risk pesticides are available to you. Note howe
                verthat the pesticide label may address other risks that need to
                be managed, or it may indicate a requirement to manage risks
                that our analysis did not consider.
              </li>
            </ul>
            <p>
              NOTE: Never assume a pesticide is safe. Our classification system
              is a guide towards risk reduction, but you must handle every
              pesticide with care, and apply it correctly at the labeled rate,
              and only on the crops and pests indicated on the label. Please
              also note that pesticide formulations vary widely in
              concentration, and the hazard that they pose to handlers.
            </p>
          </Grid>
        </Grid>
        <Grid row className="flex-justify-center">
          <Grid col={8}>
            <h3>Adapted from: </h3>
            <small>
              Jepson, P.C., Murray, K., Bach, O., Bonilla, M.A., Neumeister, L.
              Selection of pesticides to reduce human and environmental health
              risks: a global guideline and minimum pesticides list. Lancet
              Planetary Health 2020; 4: e56-63.
            </small>
            <div>
              <small>
                DOI:{" "}
                <a href="https://doi.org/10.1016/S2542-5196(19)30266-9">
                  https://doi.org/10.1016/S2542-5196(19)30266-9
                </a>
              </small>
            </div>
          </Grid>
        </Grid>
      </GridContainer>
    </Layout>
  )
}

export default UserGuid
