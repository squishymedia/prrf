import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import SearchContainer from "../containers/searchContainer"

const IndexPage = () => (
  <div className="search-page">
    <Layout>
      <SEO title="Home" />
      <SearchContainer />
    </Layout>
  </div>
)

export default IndexPage
