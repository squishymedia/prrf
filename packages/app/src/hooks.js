import { useState, useEffect, useRef, useMemo } from "react"
import fetchData from "./api/fetchData"
import { buildSubstringSearchQuery } from "./api/queries"

export function useSearchParams(searchString) {
  return Object.fromEntries(new URLSearchParams(searchString).entries())
}

export function useFetchData(query) {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])

  const queryMemo = useMemo(() => query, [JSON.stringify(query)])
  useEffect(() => {
    if (queryMemo) {
      setLoading(true)
      fetchData(query).then(results => {
        setData(results)
        setLoading(false)
      })
    }
  }, [queryMemo])

  return { loading, data }
}

const fetchSearch = async searchCtx => {
  const {
    filters: { text, riskMitigation },
    sortOrder,
  } = searchCtx
  const frontMatchQuery = buildSubstringSearchQuery({
    filters: {
      text: text + "%",
      riskMitigation,
    },
    sortOrder,
  })

  const substringMatchQuery = buildSubstringSearchQuery({
    filters: {
      text: "%" + text + "%",
      riskMitigation,
    },
    sortOrder,
  })

  try {
    const [frontMatch, substringMatch] = await Promise.all([
      fetchData(frontMatchQuery),
      fetchData(substringMatchQuery),
    ])

    for (const pesticide of substringMatch.pesticide) {
      if (!frontMatch.pesticide.find(p => p.id === pesticide.id)) {
        frontMatch.pesticide.push(pesticide)
      }
    }

    return frontMatch
  } catch (err) {
    console.error(err)
  }
}
export function useSubstringSearch(searchCtx) {
  const {
    filters: { text, riskMitigation },
    pagination: { itemsPerPage, offset },
    sortOrder: { orderBy },
    isInitialSearch,
  } = searchCtx

  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  const dataRef = useRef({})

  useEffect(() => {
    if (!isInitialSearch) {
      setLoading(true)
      fetchSearch(searchCtx).then(results => {
        dataRef.current = results
        setData({
          pesticide: dataRef.current.pesticide?.slice(
            offset,
            offset + itemsPerPage
          ),
        })
        setLoading(false)
      })
    }
  }, [text, riskMitigation, orderBy, isInitialSearch])

  useEffect(() => {
    if (!isInitialSearch) {
      setLoading(true)
      setData({
        pesticide: dataRef.current.pesticide.slice(
          offset,
          offset + itemsPerPage
        ),
      })
      setLoading(false)
    }
  }, [offset])

  return { loading, data }
}
