import React, { useState, useContext } from "react"
import Input from "../components/input"
import { useSubstringSearch } from "../hooks"
import { SearchContext } from "../state/searchContext"
import { useStaticQuery, graphql } from "gatsby"

const InputContainer = () => {
  const context = useContext(SearchContext)
  const [searchText, setSearchText] = useState("")
  const [riskMitigation, setRiskMitigation] = useState("")
  const { loading, data } = useSubstringSearch({
    filters: {
      text: searchText,
      riskMitigation,
    },
    sortOrder: {
      orderBy: "name: asc",
    },
    pagination: {
      itemsPerPage: 10,
      offset: 0,
    },
    isInitialSearch: !searchText && !riskMitigation,
  })

  const imageData = useStaticQuery(graphql`
    query Image {
      file(base: { eq: "prrf-search-header-2.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1350) {
            src
          }
        }
      }
    }
  `)

  function onSubmit(e) {
    e.preventDefault()
    context.handleFilterChange("text", searchText)
    context.handleFilterChange("riskMitigation", riskMitigation)
  }

  function handleKeyDown(e) {
    if (e.key === "Enter") {
      onSubmit(e)
    }
  }
  return (
    <Input
      loading={loading}
      results={(searchText && data.pesticide) || []}
      searchText={searchText}
      setSearchText={setSearchText}
      riskMitigation={riskMitigation}
      setRiskMitigation={setRiskMitigation}
      onSubmit={onSubmit}
      handleKeyDown={handleKeyDown}
      isInitialSearch={context.isInitialSearch}
      headerImage={imageData.file.childImageSharp.fluid.src}
    />
  )
}

export default InputContainer
