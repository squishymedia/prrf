import React, { useState, useEffect } from "react"
import Search from "../components/search"
import {
  SearchContext,
  filters,
  sortOrder,
  pagination,
  isInitialSearch,
} from "../state/searchContext"
import { buildPesticideCountQueryString } from "../api/queries"
import { useFetchData } from "../hooks.js"

const SearchContainer = () => {
  const [searchCtx, setSearchCtx] = useState({
    filters,
    sortOrder,
    pagination,
    handleFilterChange,
    handleSortOrder,
    handlePaginationChange,
    changePage,
    isInitialSearch,
  })

  const { data } = useFetchData(buildPesticideCountQueryString(searchCtx))
  const aggregate =
    data.pesticide_aggregate?.aggregate || data.search_text_aggregate?.aggregate
  const count = !searchCtx.isInitialSearch ? aggregate.count : 0

  useEffect(() => {
    handlePaginationChange("totalItems", count)
  }, [count])

  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
    })
  }, [searchCtx.pagination])

  function handleFilterChange(targetFilter, newValue) {
    setSearchCtx(searchCtx => {
      return {
        ...searchCtx,
        pagination,
        filters: {
          ...searchCtx.filters,
          [targetFilter]: newValue,
        },
        isInitialSearch: false,
      }
    })
  }

  function handleSortOrder(targetFilter, newValue) {
    setSearchCtx(searchCtx => {
      return searchCtx.sortOrder[targetFilter] === newValue
        ? searchCtx
        : {
            ...searchCtx,
            pagination: {
              ...pagination,
              totalItems: searchCtx.pagination.totalItems,
            },
            sortOrder: {
              ...searchCtx.sortOrder,
              [targetFilter]: newValue,
            },
          }
    })
  }

  function handlePaginationChange(key, newValue) {
    setSearchCtx(searchCtx => {
      return searchCtx.pagination[key] === newValue
        ? searchCtx
        : {
            ...searchCtx,
            pagination: {
              ...searchCtx.pagination,
              [key]: newValue,
            },
          }
    })
  }

  function changePage(pageNumber) {
    handlePaginationChange(
      "offset",
      searchCtx.pagination.itemsPerPage * pageNumber
    )
  }

  return (
    <SearchContext.Provider value={searchCtx}>
      <Search />
    </SearchContext.Provider>
  )
}
export default SearchContainer
