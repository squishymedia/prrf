import React from "react"
import Results from "../components/results"
import Loader from "../components/loader"
import { SearchContext } from "../state/searchContext"
import { useSubstringSearch } from "../hooks"

const ResultsContainer = searchCtx => {
  const {
    filters: { text, riskMitigation },
    isInitialSearch,
  } = searchCtx

  const { loading, data } = useSubstringSearch(searchCtx)

  const startingPage = searchCtx.pagination.offset + 1
  const currentlyShowing =
    (searchCtx.pagination.offset / searchCtx.pagination.itemsPerPage + 1) *
    searchCtx.pagination.itemsPerPage
  const totalItems = searchCtx.pagination.totalItems

  const searchParams = [
    text && (
      <>
        "<b>{text}</b>"
      </>
    ),
    " ",
    riskMitigation && (
      <>
        and <em>Mitigation Requirement</em> "
        <b>
          {riskMitigation
            .split("_")
            .map(word => word[0].toUpperCase() + word.slice(1))
            .join(" ")}
        </b>
        "
      </>
    ),
  ]
  const resultCount =
    searchCtx.pagination.totalItems > 0 ? (
      <div>
        Results:<b> {startingPage} </b>
        <span>- </span>
        <b>{currentlyShowing > totalItems ? totalItems : currentlyShowing}</b>
        <span> of </span>
        <b>{totalItems}</b>
        {(text || riskMitigation) && <span> Matching {searchParams}</span>}
      </div>
    ) : null

  return isInitialSearch ? null : loading ? (
    <Loader />
  ) : (
    <Results
      results={data.pesticide || data.search_text}
      resultCount={resultCount}
      filters={searchCtx.filters}
      sortOrder={searchCtx.sortOrder}
      sort={searchCtx.handleSortOrder}
    />
  )
}

export default () => (
  <SearchContext.Consumer>
    {context => <ResultsContainer {...context} />}
  </SearchContext.Consumer>
)
