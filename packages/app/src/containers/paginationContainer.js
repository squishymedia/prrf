import React, { useContext } from "react"
import Pagination from "../components/pagination"
import { SearchContext } from "../state/searchContext"

const PaginationContainer = () => {
  const {
    changePage,
    pagination: { itemsPerPage, offset, totalItems },
    isInitialSearch
  } = useContext(SearchContext)
  const numPages = Math.ceil(totalItems / itemsPerPage)
  const currentPage = offset / itemsPerPage
  return (
    <Pagination
      changePage={changePage}
      numPages={numPages}
      currentPage={currentPage}
      isInitialSearch={isInitialSearch}
    />
  )
}

export default PaginationContainer
