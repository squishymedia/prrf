import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import {
  GridContainer,
  Grid,
  Label,
  //Dropdown
} from "@trussworks/react-uswds"
import Highlighter from "react-highlight-words"
import InitialSearchText from "../components/initialSearchText"
import { css } from "@emotion/react"

const Input = ({
  results,
  searchText,
  setSearchText,
  // riskMitigation,
  // setRiskMitigation,
  headerImage,
  handleKeyDown,
  onSubmit,
  isInitialSearch,
}) => {
  return (
    <section
      id="search-header"
      className={isInitialSearch ? "section initial-search" : "section"}
      css={css`
        ${{
          background: `url(${headerImage})`,
        }};
      `}
    >
      <GridContainer>
        {isInitialSearch ? <InitialSearchText /> : null}
        <Grid
          row
          className="flex-justify-center flex-align-end padding-top-1 padding-bottom-4"
        >
          <Grid
            tablet={{ col: true }}
            className="margin-right-1 search-input-grid-wrap"
          >
            <div autoComplete="off" className="usa-search  ">
              <Label htmlFor="options" className="font-sans-md">
                Pesticides by active ingredient
              </Label>
              <input
                aria-label="Search input for pesticides"
                className="usa-input margin-top-1"
                id="search-field"
                type="search"
                name="search"
                autoComplete="off"
                onChange={e => setSearchText(e.target.value)}
                onKeyDown={handleKeyDown}
                value={searchText}
              />
              <ul className="search-sub">
                {results.map(result => (
                  <li key={result.id}>
                    <Link to={`/pesticide-detail?id=${result.id}`}>
                      <Highlighter
                        highlightClassName="searchHighlight bg-yellow"
                        searchWords={[searchText]}
                        autoEscape={true}
                        textToHighlight={result.name}
                      />
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </Grid>
          {/* <Grid
            tablet={{ col: true }}
            className="margin-right-1 search-filter-grid-wrap"
          >
            <Label htmlFor="input-dropdown">
              Mitigation requirement <small>(optional)</small>
            </Label>
            <Dropdown
              aria-label="Filter by mitigation requirements"
              className="input-dropdown"
              name="input-dropdown"
              onChange={e => setRiskMitigation(e.target.value)}
              value={riskMitigation}
            >
              <option value="">None</option>
              <option value="aquatic">Aquatic</option>
              <option value="better_personal_protection">
                Better Personal Protection
              </option>
              <option value="bystander">Bystander</option>
              <option value="pollinator">Pollinator</option>
              <option value="wildlife">Wildlife</option>
            </Dropdown>
          </Grid> */}
          {/*
            *** HIDDEN - WAITING FOR CONTENT
            ***
           <Grid tablet={{ col: 3 }} className="margin-right-1">
            <Label htmlFor="options">
              <small>Mode of Action</small>
            </Label>
            <Dropdown className="input-dropdown" name="input-dropdown">
              <option>- Select - </option>
              <option value="value1">Option A</option>
            </Dropdown>
          </Grid>
          <Grid tablet={{ col: 3 }}>
            <Label htmlFor="options">
              <small>Pesticide Type</small>
            </Label>
            <Dropdown className="input-dropdown" name="input-dropdown">
              <option>- Select - </option>
              <option value="value1">Option A</option>
            </Dropdown>
          </Grid> 
          */}
          <Grid tablet={{ col: "auto" }} className="search-button-grid-wrap">
            <button className="usa-button" onClick={onSubmit}>
              Search
            </button>
          </Grid>
        </Grid>
      </GridContainer>
    </section>
  )
}

Input.propTypes = {
  results: PropTypes.array,
  searchText: PropTypes.string,
  setSearchText: PropTypes.func,
  riskMitigation: PropTypes.string,
  setRiskMitigation: PropTypes.func,
  onSubmit: PropTypes.func,
  handleKeyDown: PropTypes.func,
  isInitialSearch: PropTypes.bool,
  headerImage: PropTypes.string,
}

export default Input
