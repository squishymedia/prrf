/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import Footer from "./footer"

import "@trussworks/react-uswds/lib/uswds.css"
import "@trussworks/react-uswds/lib/index.css"
import "../scss/app.scss"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          mainNavigation {
            label
            path
          }
          brandPartners {
            label
            path
            logo
            logoFooter
            logoClassName
            hideInMainNav
          }
          footerInfo {
            someText
          }
          footerNavigation {
            label
            path
          }
          footerSecondaryNavigation {
            label
            path
          }
        }
      }
    }
  `)

  return (
    <>
      <div className="content-wrap">
        <Header
          siteTitle={data.site.siteMetadata.title}
          siteMetadata={data.site.siteMetadata}
        />

        <main id="main-content">{children}</main>
      </div>

      <Footer
        siteTitle={data.site.siteMetadata.title}
        siteMetadata={data.site.siteMetadata}
        year={new Date().getFullYear()}
      />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
