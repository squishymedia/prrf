import React from "react"
import { Link } from "gatsby"
import HazardList from "../components/hazardList"
import RiskMitigationList from "../components/riskMitigationList"
import { GridContainer, Grid } from "@trussworks/react-uswds"
import { capitalized } from "../utils"

export default function PesticideDetail({ pesticide }) {
  const cas_number = pesticide.cas_number
  const name = pesticide.name

  let riskLevel = "",
    riskSectionClassName = "",
    riskBorderColor = "",
    modalHeader = "",
    modalContent = ""
  switch (pesticide.risk_table) {
    case "1":
      riskLevel = "Highly Hazardous"
      riskSectionClassName = "risk-level-hhp bg-secondary-light"
      riskBorderColor = "border-secondary-light"
      modalHeader =
        "Table 1 Highly hazardous pesticides (HHPs), with criteria for classification:"
      modalContent =
        "Definitions of HHP criteria provided in text, with personal protection and risk management requirements for essential uses during HHP phase-out. Pesticides listed as obsolete are not fit for further use, and may also have been deregistered locally, or banned internationally."
      break
    case "2":
      riskLevel = "Requires PPE"
      riskSectionClassName = "risk-level-ppe bg-accent-warm-light"
      riskBorderColor = "border-accent-warm-light"
      modalHeader =
        "Table 2 Pesticides requiring risk mitigation (see text for details of mitigation methods)."
      modalContent =
        "Only use these products if you are trained, and you have access to high quality personal protective equipment (PPE) for mixing, loading, handling, application, clean-up, and during the of restricted entry period. “Higher-level personal protection required” (Π) indicates that occupational exposure risk assessments have demonstrated potential for exposure, and significant acute or chronic risks. If labels do not provide details of PPE for applicators, a precautionary PPE recommendation is: wear coveralls over long- sleeved shirt, long pants, socks and stout shoes, with chemically resistant gloves, with protection for eyes (i.e. a face mask or goggles), and respiratory protection (i.e. a respirator). For pesticides labeled as having a requirement to mitigate bystander risk, wear respirators with an organic vapor (OV) cartridge or canister with any N, R, P, or 100-series filter. Some of these compounds require additional engineering risk mitigations such as closed mixing, and closed cabs, and are not suitable for smallholder farmer use. Please obey all restricted entry and pre-harvest intervals indicated in the label. "
      break
    case "3a":
      riskLevel = "Low Risk"
      riskSectionClassName = "risk-level-low bg-accent-cool-light"
      riskBorderColor = "border-accent-cool-light"
      modalHeader = "Table 3a"
      modalContent =
        "Pesticides that do not trigger environmental or bystander risk mitigations, but which require additional occupational health risk mitigation, in addition to baseline personal protective equipment (PPE). Occupational exposure risk assessments have demonstrated potential for exposure, and significant acute or chronic risks. If labels do not provide details of PPE for applicators, a precautionary PPE recommendation is: wear long-sleeved shirt, long pants, socks and stout shoes, with chemically resistant gloves, with protection for eyes (i.e. a face mask or goggles), and respiratory protection (i.e. a respirator). Please obey all restricted entry and pre-harvest intervals indicated in the label. Also, consider every chemical on a case-by- case basis, and obtain local regulatory data, and research that address aspects of risk that this system does not take into account. "
      break
    case "3b":
      riskLevel = "Low Risk"
      riskSectionClassName = "risk-level-low bg-accent-cool-light"
      riskBorderColor = "border-accent-cool-light"
      modalHeader = "Table 3b "
      modalContent =
        "Pesticides that do not meet the HHP, risk mitigation, or other of criteria of concern in Tables 1, 2 & 3a, but which require single layer, baseline, personal protective equipment (PPE): i.e. wear long-sleeved shirt, long pants, socks and stout shoes, with chemically resistant gloves. Note: occupational exposure is unlikely for pheromones. Consider every chemical on a case-by-case basis, and obtain local regulatory data, and research that addresses aspects of risk that this system does not take into account, including use of products for self harm."
      break
    default:
      riskLevel = ""
      riskSectionClassName = "risk-level-none"
      riskBorderColor = ""
      modalHeader = ""
      modalContent = ""
  }
  const noMitigation = () => {
    for (let i in pesticide.risk_mitigation) {
      if (pesticide.risk_mitigation[i]) return true
    }
    return false
  }

  const [isOpen, setIsOpen] = React.useState(false)

  return (
    <div className="pesticide-detail">
      <section id="header" className={"usa-section " + riskSectionClassName}>
        <GridContainer>
          <Grid row>
            <Grid tablet={{ col: true }}>
              <Grid row>
                <Grid tablet={{ col: 8 }} className="margin-right-5">
                  <div className="font-sans-xs margin-bottom-2">
                    <b>CAS#</b> {cas_number}
                  </div>
                  <h2 className="font-heading-xl margin-y-0">{name}</h2>
                </Grid>
                <Grid tablet={{ col: true }} className="text-right">
                  <h3
                    className="risk-level-label font-alt-lg risk-header"
                    onClick={() => setIsOpen(prevIsOpen => !prevIsOpen)}
                  >
                    {riskLevel}
                  </h3>
                  {pesticide.hazard !== null ? (
                    <HazardList data={pesticide.hazard} />
                  ) : null}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </GridContainer>
      </section>
      <section id="content" className="section padding-y-3">
        <GridContainer>
          <Grid row className="flex-justify-center">
            <Grid tablet={{ col: 12 }}>
              <div className="classification">
                <h2 className="grid-row flex-align-center font-alt-lg">
                  {pesticide.pesticide_type?.type} -{" "}
                  {pesticide.chemical_classification && (
                    <>{capitalized(pesticide.chemical_classification.class)}</>
                  )}
                  {pesticide.chemical_classification?.sub_class && (
                    <>
                      /{" "}
                      {capitalized(
                        pesticide.chemical_classification?.sub_class
                      )}
                    </>
                  )}
                </h2>
              </div>
              <div className="mode-of-action grid-row flex-align-center">
                <h2 className="margin-right-1 font-alt-md">Mode of Action:</h2>
                <span className="margin-right-1 font-alt-md">
                  {pesticide.mode_of_action?.code || "n/a"}
                </span>
                {pesticide.mode_of_action?.term && (
                  <span className="font-alt-md">
                    <span>
                      {" "}
                      - {capitalized(pesticide.mode_of_action?.term) || ""}
                    </span>
                  </span>
                )}
              </div>
              {/* <div className="net-rating flex-align-center flex-justify-between ">
                    <h3 className="margin-bottom-0 font-sans-md">
                      <span className="text-tabular">00%</span> Natural Enemy
                      Toxicity Rating
                    </h3>
                  </div> */}
            </Grid>

            {noMitigation() ? (
              <Grid
                tablet={{ col: 12 }}
                className={
                  riskBorderColor +
                  " risk-mitigation border-top-1 margin-top-2 padding-top-2"
                }
              >
                <h2 className="font-alt-md">
                  <Link
                    href={
                      pesticide.risk_table === "1"
                        ? "/risk-mitigation"
                        : "/requires-mitigation"
                    }
                  >
                    Requires {pesticide.risk_table === "1" ? "Risk" : ""}{" "}
                    Mitigation
                  </Link>
                </h2>
                <Grid row>
                  <RiskMitigationList data={pesticide.risk_mitigation} />
                </Grid>
                <Link className="margin-top-3" href="/user-guide">
                  User Guide
                </Link>
              </Grid>
            ) : null}
          </Grid>
        </GridContainer>
      </section>
      <div className={isOpen ? "modal-dialog modal-open" : "modal-dialog"}>
        <div className="modal-content">
          <span
            className="modal-close"
            onClick={() => setIsOpen(prevIsOpen => !prevIsOpen)}
          >
            x
          </span>
          <div className="modal-main">
            <h2 className="modal-heading">{modalHeader}</h2>
            <div className="line-height-mono-4">{modalContent}</div>
          </div>
          <span
            className="modal-close-button"
            onClick={() => setIsOpen(prevIsOpen => !prevIsOpen)}
          >
            Close
          </span>
        </div>
      </div>
    </div>
  )
}
