import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { GridContainer, Grid } from "@trussworks/react-uswds"

const SiteFooter = ({ siteTitle, siteMetadata, year }) => {
  return (
    <section className="footer padding-y-3 usa-section margin-y-0">
      <GridContainer>
        <Grid row className="flex-align-center footer-primary margin-y-0">
          <Grid tablet={{ col: true }}>
            <h2 className="font-sans-lg margin-top-0">{siteTitle}</h2>
          </Grid>
        </Grid>
        <Grid row gap className="flex-align-center margin-y-4">
          <Grid tablet={{ col: "12" }}>
            <h3>Project Partners:</h3>
          </Grid>
          {siteMetadata.brandPartners.map((link, index) => (
            <Grid
              tablet={{ col: "auto" }}
              key={index}
              className="brand-partners-footer"
            >
              <a index={index} href={link.path}>
                <img
                  className={link.logoClassName}
                  src={link.logoFooter}
                  alt={link.label + " brand icon"}
                />
              </a>
            </Grid>
          ))}
        </Grid>
        <Grid row className="footer-secondary">
          <Grid tablet={{ col: true }}>
            <Grid row gap className="flex-align-center flex-justify-start">
              <Grid tablet={{ col: "auto" }} className="font-sans-2xs">
                © {year} PRRF / USAID / USDA
              </Grid>
              {siteMetadata.footerSecondaryNavigation.map((link, index) => (
                <Grid tablet={{ col: "auto" }} key={index}>
                  <Link index={index} to={link.path} className="font-sans-2xs">
                    {link.label}
                  </Link>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </GridContainer>
    </section>
  )
}

SiteFooter.propTypes = {
  siteTitle: PropTypes.string,
  siteMetadata: PropTypes.object,
}

SiteFooter.defaultProps = {
  siteTitle: ``,
}

export default SiteFooter
