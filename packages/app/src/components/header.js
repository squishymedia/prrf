import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import {
  Header,
  Title,
  NavMenuButton,
  PrimaryNav,
  Grid,
} from "@trussworks/react-uswds"

const SiteHeader = ({ siteTitle, siteMetadata }) => {
  const [expanded, setExpanded] = React.useState(false)
  const onClick = () => setExpanded(prvExpanded => !prvExpanded)

  return (
    <>
      <a className="usa-skipnav" href="#main-content">
        Skip to main content
      </a>
      <Header basic={true}>
        <div className="padding-y-2">
          <Grid row className="flex-align-center padding-x-3">
            <Grid className="grid-col-fill">
              <div>
                <Title className="margin-y-0">
                  <a href="/">{siteTitle}</a>
                </Title>
                <div className="subhead">
                  A collaborative project between OSU, USAID, and USDA
                </div>
              </div>
            </Grid>
            <Grid className="grid-col-auto margin-left-auto">
              <NavMenuButton onClick={onClick} label="Menu" />
              <Grid row className="grid-gap flex-align-center">
                <Grid tablet={{ col: true }}>
                  <Grid
                    row
                    className="flex-align-center flex-justify-end brand-nav"
                  >
                    {siteMetadata.brandPartners.map((link, index) =>
                      !link.hideInMainNav ? (
                        <Grid
                          tablet={{ col: "auto" }}
                          className="padding-left-2"
                          key={index}
                        >
                          <a href={link.path}>
                            <img
                              className={link.logoClassName}
                              src={link.logo}
                              alt={link.label + " brand icon"}
                            />
                          </a>
                        </Grid>
                      ) : null
                    )}
                  </Grid>
                </Grid>
                <Grid tablet={{ col: 2 }} className="primary-nav-wrap">
                  <PrimaryNav
                    items={siteMetadata.mainNavigation.map((link, index) => (
                      <Link key={index} to={link.path}>
                        {link.label}
                      </Link>
                    ))}
                    mobileExpanded={expanded}
                    onToggleMobileNav={onClick}
                  ></PrimaryNav>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Header>
      <div className={`usa-overlay ${expanded ? "is-visible" : ""}`}></div>
    </>
  )
}

SiteHeader.propTypes = {
  siteTitle: PropTypes.string,
  siteMetadata: PropTypes.object,
}

SiteHeader.defaultProps = {
  siteTitle: ``,
}

export default SiteHeader
