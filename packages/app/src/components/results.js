import React from "react"
import { GridContainer, Grid, Dropdown, Label } from "@trussworks/react-uswds"
import SingleResult from "./singleResult"
import NoResults from "./noSearchResultsText"
import PropTypes from "prop-types"

const Results = ({ results, filters, resultCount, sortOrder, sort }) => {
  return (
    <>
      <section id="sort" className="section padding-top-1 padding-bottom-2">
        <GridContainer>
          <Grid row className="flex-align-center">
            <Grid tablet={{ col: "auto" }}>{resultCount}</Grid>
            <Grid
              tablet={{ col: "auto" }}
              className="margin-left-auto sort-drop"
            >
              <Grid row className="flex-align-center">
                <Grid tablet={{ col: "auto" }}>
                  <Label htmlFor="sort-dropdown" className="margin-top-1">
                    <small>Sort: &nbsp;</small>
                  </Label>
                </Grid>
                <Grid tablet={{ col: "auto" }}>
                  <Dropdown
                    aria-label="Sort options for results"
                    className="input-dropdown"
                    name="sort-dropdown"
                    onChange={e => sort("orderBy", e.target.value)}
                    value={sortOrder.orderBy}
                  >
                    <option value="name: asc">Name - A to Z</option>
                    <option value="name: desc">Name - Z to A</option>
                    <option value="risk_table: desc, name: asc">
                      Risk - Low to High
                    </option>
                    <option value="risk_table: asc, name: asc">
                      Risk - High to Low
                    </option>
                  </Dropdown>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </GridContainer>
      </section>
      <section id="results" className="section">
        <SingleResult results={results} filters={filters} />
        <GridContainer>
          <Grid row>
            <Grid tablet={{ col: true }}>
              {results.length === 0 ? <NoResults /> : null}
            </Grid>
          </Grid>
          <Grid row className="flex-justify-center margin-top-1">
            {resultCount}
          </Grid>
        </GridContainer>
      </section>
    </>
  )
}

Results.propTypes = {
  results: PropTypes.array,
  resultCount: PropTypes.object,
  sort: PropTypes.func,
  filters: PropTypes.object,
}

export default Results
