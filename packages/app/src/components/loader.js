import React from "react"

const Loader = () => {
  return (
    <div className="loadingWrap">
      <b>Loading...</b>
      <div className="loading_1"></div>
      <div className="loading_2"></div>
      <div className="loading_3"></div>
      <div className="loading_4"></div>
      <div className="loading_5"></div>
    </div>
  )
}

export default Loader
