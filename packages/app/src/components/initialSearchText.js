import React from "react"
import { Grid } from "@trussworks/react-uswds"
const initialSearchText = () => {
  return (
    <Grid row>
      <Grid>
        <h1 className="margin-top-0 padding-top-3 padding-bottom-2 font-sans-xl">
          PRRF classifies the risks and hazards of over 600 pesticides with
          respect to human and environmental health.
        </h1>{" "}
        <p className="font-sans-lg">
          Enter a search criteria below to get started.
        </p>
      </Grid>
    </Grid>
  )
}

export default initialSearchText
