import React from "react"
import { GridContainer } from "@trussworks/react-uswds"
const noSearchResults = () => {
  return (
    <section id="results" className="section padding-top-3">
      <GridContainer>
        <div>
          <h1>No results</h1>
          <p>Please update your search to get more results</p>
        </div>
      </GridContainer>
    </section>
  )
}

export default noSearchResults
