import React from "react"
import InputContainer from "../containers/inputContainer"
import ResultsContainer from "../containers/resultsContainer"
import PaginationContainer from "../containers/paginationContainer"

const Search = () => (
  <>
    <InputContainer />
    <ResultsContainer />
    <PaginationContainer />
  </>
)

export default Search
