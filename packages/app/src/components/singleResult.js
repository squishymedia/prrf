import React from "react"
import { Link } from "gatsby"
import Highlighter from "react-highlight-words"
import RiskMitigationList from "./riskMitigationList"
import { GridContainer, Grid } from "@trussworks/react-uswds"
import { capitalized } from "../utils"

const SingleResult = ({ results, filters }) => {
  return (
    <>
      {results.map((node, index) => {
        const name = node.name
        const casNumber = node.cas_number
        const id = node.id
        const riskMitigation =
          node.risk_mitigation !== null ? (
            <RiskMitigationList
              key={name}
              data={node.risk_mitigation}
              type={"teaser"}
            />
          ) : null
        const type = capitalized(node.pesticide_type?.type)
        const modeOfActionNumber = node.mode_of_action?.code

        let riskLevel = "",
          riskSectionClassName = ""
        switch (node.risk_table) {
          case "1":
            riskLevel = "Highly Hazardous"
            riskSectionClassName = "risk-level-hhp bg-secondary-light"
            break
          case "2":
            riskLevel = "Requires PPE"
            riskSectionClassName = "risk-level-ppe bg-accent-warm-light"
            break
          case "3a":
          case "3b":
            riskLevel = "Low Risk"
            riskSectionClassName = "risk-level-low bg-accent-cool-light"
            break
          default:
            riskLevel = ""
            riskSectionClassName = "risk-level-none"
        }

        return (
          <div className="result-row padding-4" key={index}>
            <GridContainer>
              <Grid row>
                <Grid tablet={{ col: true }}>
                  <small>
                    <b>CAS# </b>
                    <Highlighter
                      highlightClassName="searchHighlight bg-yellow"
                      searchWords={[filters.text]}
                      autoEscape={true}
                      textToHighlight={
                        casNumber === "" ? "no-cas-number" : casNumber
                      }
                    />
                  </small>
                  <Grid row className="flex-align-center">
                    <Grid tablet={{ col: "auto" }}>
                      <h3 className="usa-card__heading margin-y-0 margin-right-2 font-sans-lg">
                        <Link to={"/pesticide-detail?id=" + id}>
                          <Highlighter
                            highlightClassName="searchHighlight bg-yellow"
                            searchWords={[filters.text]}
                            autoEscape={true}
                            textToHighlight={name === "" ? "no-name" : name}
                          />
                        </Link>
                      </h3>
                    </Grid>
                    <Grid tablet={{ col: true }}>
                      <span className="font-sans-md">
                        MoA: {modeOfActionNumber || "n/a"}
                      </span>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid>
                  <Grid row className="flex-justify-end risk-tag">
                    <span
                      className={"usa-tag padding-y-1 " + riskSectionClassName}
                    >
                      {riskLevel}
                    </span>
                  </Grid>
                  <Grid row className="flex-justify-end pesticide-type">
                    <span className=" margin-top-2 ">
                      {type}
                    </span>
                  </Grid>
                  <Grid
                    row
                    className="flex-justify-end margin-top-2 risk-mitigation"
                  >
                    {riskMitigation}
                  </Grid>
                </Grid>
              </Grid>
            </GridContainer>
          </div>
        )
      })}
    </>
  )
}

export default SingleResult
