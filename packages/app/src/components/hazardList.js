import React from "react"

export default function HazardList({ data }) {
  const hazardlist = Object.entries(data).map(([key, value]) => {
    const keyStr = key
    let newKey = ""
    if (keyStr === "who_1a") {
      newKey = "WHO 1a"
    } else if (keyStr === "who_1b") {
      newKey = "WHO 1b"
    } else {
      newKey = keyStr.replace(/_/g, " ")
    }
    return value === true ? (
      <div className="hazardListItem" key={key}>
        <span>
          <b>{newKey}</b>
        </span>
      </div>
    ) : null
  })
  return hazardlist
}
