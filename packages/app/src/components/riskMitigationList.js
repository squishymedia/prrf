import React from "react"
import ReactTooltip from "react-tooltip"
import { Grid } from "@trussworks/react-uswds"

import BeeSvg from "../../images/svg/bee.svg"
import HawkSvg from "../../images/svg/hawk.svg"
import HazmatSvg from "../../images/svg/hazmat.svg"
import WalkSvg from "../../images/svg/walk.svg"
import FishSvg from "../../images/svg/fish.svg"

export default function RiskMitigationList({ data, type }) {
  return Object.entries(data).map(([key, value], index) => {
    const keyStr = key
    const newKey = keyStr.replace(/_/g, " ")
    let toolTipText = "",
      toolTipSVG
    switch (key) {
      case "aquatic":
        toolTipText = "Aquatic"
        toolTipSVG = FishSvg
        break
      case "better_personal_protection":
        toolTipText = "Better Personal Protection"
        toolTipSVG = HazmatSvg
        break
      case "bystander":
        toolTipText = "bystander"
        toolTipSVG = WalkSvg
        break
      case "pollinator":
        toolTipText = "pollinator"
        toolTipSVG = BeeSvg
        break
      case "wildlife":
        toolTipText = "wildlife"
        toolTipSVG = HawkSvg
        break
      default:
        toolTipText = ""
        toolTipSVG = ""
    }

    return value === true ? (
      <>
        {type === "teaser" ? (
          <div className="risk-mitigation-wrap" key={index}>
            <span data-tip={toolTipText}>
              <Grid tablet={{ col: "auto" }}>
                <img
                  src={toolTipSVG}
                  alt={"black and white " + newKey + " illustration"}
                  className="margin-left-1 square-3"
                />
              </Grid>
              <ReactTooltip place="top" type="dark" effect="float" />
            </span>
          </div>
        ) : (
          <div className="risk-mitigation-wrap margin-right-5" key={index}>
            <span>
              <Grid
                row
                className="grid-row flex-align-center flex-justify-end margin-top-2"
              >
                <Grid tablet={{ col: "auto" }}>
                  <img
                    src={toolTipSVG}
                    alt={"black and white " + newKey + " illustration"}
                    className="square-3"
                  />
                </Grid>
                <Grid className="margin-left-1" tablet={{ col: "auto" }}>
                  {" "}
                  {newKey}
                </Grid>
              </Grid>
            </span>
          </div>
        )}
      </>
    ) : null
  })
}
