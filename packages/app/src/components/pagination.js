import React from "react"
import PropTypes from 'prop-types'
import { GridContainer, Grid } from "@trussworks/react-uswds"

const Pagination = ({ changePage, numPages, currentPage, isInitialSearch }) => {
  return (
    <section
      className={`usa-section section padding-${
        isInitialSearch ? "0" : "bottom-3"
      }`}
    >
      <GridContainer>
        <Grid row className="flex-justify-center">
          <Grid tablet={{ col: "auto" }}>
            <ul className="usa-button-group usa-button-group--segmented">
              {Array.from({ length: numPages }).map((_, index) => {
                return (
                  <li className="usa-button-group__item" key={index}>
                    <button
                      className={`usa-button${
                        index !== currentPage ? " usa-button--outline" : ""
                      }`}
                      value={index}
                      onClick={e => changePage(e.target.value)}
                    >
                      {index + 1}
                    </button>
                  </li>
                )
              })}
            </ul>
          </Grid>
        </Grid>
      </GridContainer>
    </section>
  )
}

Pagination.propTypes = {
  changePage: PropTypes.func,
  numPages: PropTypes.number,
  currentPage: PropTypes.number,
  isInitialSearch: PropTypes.bool
}

export default Pagination
