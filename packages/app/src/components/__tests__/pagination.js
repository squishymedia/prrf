import React from "react"
import renderer from "react-test-renderer"

import Pagination from "../pagination"

describe("Pagination", () => {
  it("renders correctly", () => {
    const testProps = {
      changePage: () => { },
      numPages: 5,
      currentPage: 2,
      isInitialSearch: false
    }

    const tree = renderer
      .create(
        <Pagination
          changePage={testProps.changePage}
          numPages={testProps.numPages}
          currentPage={testProps.currentPage}
          isInitialSearch={testProps.isInitialSearch}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
