import React from "react"
import renderer from "react-test-renderer"

import InitialSearchText from "../initialSearchText"

describe("InitialSearchText", () => {
  it("renders correctly", () => {
    const tree = renderer.create( <InitialSearchText />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
