import React from "react"
import renderer from "react-test-renderer"
import { removeClassNamesFromTree } from "../../testUtils"

import Results from "../results"

describe("Results", () => {
  it("renders correctly", () => {
    const testProps = {
      filters: {
        text: "dic",
        riskMitigation: "pollinator",
      },
      results: [
        {
          cas_number: "542-75-6",
          id: 1,
          name: "1,3-Dichloropropene",
          risk_table: "2",
          risk_mitigation: {
            aquatic: true,
            better_personal_protection: true,
            bystander: true,
            pollinator: true,
            wildlife: true,
          },
        },
        {
          cas_number: "59669-26-0",
          id: 152,
          name: "Thiodicarb",
          risk_table: "2",
          risk_mitigation: {
            aquatic: true,
            better_personal_protection: true,
            bystander: true,
            pollinator: true,
            wildlife: true,
          },
        },
      ],
      resultCount: <div>Result Count</div>,
      sortOrder: { orderBy: "risk_table: desc, name: asc" },
      sort: () => {},
    }
    const tree = renderer
      .create(
        <Results
          results={testProps.results}
          filters={testProps.filters}
          resultCount={testProps.resultCount}
          sortOrder={testProps.sortOrder}
          sort={testProps.sort}
        />
      )
      .toJSON()

    removeClassNamesFromTree(tree)

    expect(tree).toMatchSnapshot()
  })
})
