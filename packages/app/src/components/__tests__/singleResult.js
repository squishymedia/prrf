import React from "react"
import renderer from "react-test-renderer"
import { removeClassNamesFromTree } from "../../testUtils"

import SingleResult from "../singleResult"

describe("SingleResult", () => {
  it("renders correctly", () => {
    const testProps = {
      filters: {
        text: "di",
        riskMitigation: "bystander",
      },
      results: [
        {
          cas_number: "333-41-5",
          id: 45,
          name: "Diazinon",
          risk_mitigation: {
            aquatic: true,
            better_personal_protection: true,
            bystander: true,
            pollinator: true,
            wildlife: true,
          },
          risk_table: "2",
        },
        {
          cas_number: "99-30-9",
          id: 46,
          name: "Dichloran",
          risk_mitigation: {
            aquatic: false,
            better_personal_protection: true,
            bystander: true,
            pollinator: false,
            wildlife: true,
          },
          risk_table: "2",
        },
      ],
    }
    const tree = renderer
      .create(
        <SingleResult results={testProps.results} filters={testProps.filters} />
      )
      .toJSON()

    removeClassNamesFromTree(tree)

    expect(tree).toMatchSnapshot()
  })
})
