import React from "react"
import renderer from "react-test-renderer"

import Input from "../input"

describe("Input", () => {
  it("renders correctly", () => {
    const testProps = {
      results: [],
      searchText: "",
      setSearchText: () => {},
      riskMitigation: "",
      setRiskMitigation: () => {},
      handleKeyDown: () => {},
      onSubmit: () => {},
      isInitialSearch: false,
      headerImage: "",
    }

    const tree = renderer
      .create(
        <Input
          results={testProps.results}
          searchText={testProps.searchText}
          setSearchText={testProps.setSearchText}
          riskMitigation={testProps.riskMitigation}
          setRiskMitigation={testProps.setRiskMitigation}
          handleKeyDown={testProps.handleKeyDown}
          onSubmit={testProps.onSubmit}
          isInitialSearch={testProps.isInitialSearch}
          headerImage={testProps.headerImage}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
