import React from "react"
import renderer from "react-test-renderer"
import { removeClassNamesFromTree } from '../../testUtils'

import RiskMitigationList from "../riskMitigationList"

describe("RiskMitigationList", () => {
  it("renders correctly", () => {
    const testProps = {
      data: {
        aquatic: true,
        better_personal_protection: true,
      },
      type: null,
    }
    const tree = renderer
      .create(
        <RiskMitigationList data={testProps.data} type={testProps.type} />
      )
      .toJSON()
    
    removeClassNamesFromTree(tree)
    
    expect(tree).toMatchSnapshot()
  })
})
