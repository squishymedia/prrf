import React from "react"
import renderer from "react-test-renderer"

import HazardList from "../hazardList"

describe("HazardList", () => {
  it("renders correctly", () => {
    const testProps = {
      who_1a: true,
    }
    const tree = renderer.create(<HazardList data={testProps} />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
