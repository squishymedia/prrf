import React from "react"
import renderer from "react-test-renderer"

import NoSearchResults from "../noSearchResultsText"

describe("NoSearchResults", () => {
  it("renders correctly", () => {
    const tree = renderer.create(<NoSearchResults />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
