import React from "react"
import renderer from "react-test-renderer"
import { removeClassNamesFromTree } from '../../testUtils'

import PesticideDetail from "../pesticideDetail"

describe("PesticideDetail", () => {
  it("renders correctly", () => {
    const testProps = {
      cas_number: "542-75-6",
      id: 1,
      name: "1,3-Dichloropropene",
      hazard: null,
      obsolete: null,
      risk_mitigation: {
        aquatic: true,
        better_personal_protection: true,
        bystander: true,
        id: 1,
        id_pesticide: 1,
        pollinator: true,
        wildlife: true,
      },
      risk_table: "2",
      synonyms: null,
    }

    const tree = renderer
      .create(<PesticideDetail pesticide={testProps} />)
      .toJSON()

    removeClassNamesFromTree([tree])

    expect(tree).toMatchSnapshot()
  })
})
