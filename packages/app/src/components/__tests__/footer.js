import React from "react"
import renderer from "react-test-renderer"

import Footer from "../footer"

describe("Footer", () => {
  it("renders correctly", () => {
    const testProps = {
      title: "Pesticide Risk Reduction Framework",
      description: "PRRF Description",
      author: "@gatsbyjs",
      brandPartners: [
        {
          label: "OSU",
          path: "https://oregonstate.edu/",
          logo: "/osu-logo.png",
          logoFooter: "/osu.png",
          logoClassName: "width-card",
        },
        {
          label: "USAID",
          path: "https://usaid.gov/",
          logo: "/usaid-logo.svg",
          logoFooter: "/usaid.png",
          logoClassName: "width-card",
        },
        {
          label: "USDA",
          path: "https://www.usda.gov/",
          logo: "/usda-logo.png",
          logoFooter: "/usda.png",
          logoClassName: "width-8",
        },
        {
          label: "Squishymeda",
          path: "https://sqm.io/",
          hideInMainNav: true,
          logoFooter: "/sqm-logo.svg",
          logoClassName: "width-8",
        },
      ],
      mainNavigation: [
        {
          label: "Search",
          path: "/",
        },
      ],
      footerInfo: {
        someText: "this is some text",
      },
      footerNavigation: [
        {
          label: "Home",
          path: "/",
        },
        {
          label: "Page-2",
          path: "/page-2",
        },
      ],

      footerSecondaryNavigation: [
        {
          label: "Terms",
          path: "/",
        },
        {
          label: "Privacy",
          path: "/",
        },
      ],
    }

    const tree = renderer
      .create(
        <Footer
          siteTitle={testProps.title}
          siteMetadata={testProps}
          year={"2020"}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
