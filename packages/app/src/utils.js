/**
 * Capitalizes each word in a string, unless that word is ignored.
 * If the string is not a string, returns the given value as is.
 * @param {string} str - e.g. "mode of action"
 * @param {[string]} ignored - e.g. ["of"] (default, [])
 * @returns str - e.g. "Mode of Action"
 */
export const capitalized = (str, ignored=[]) => typeof str === 'string'
  ? str.split(' ').map(
    word => ignored.includes(word)
      ? word
      : word.charAt(0).toUpperCase() + word.slice(1)
  ).join(' ')
  : str