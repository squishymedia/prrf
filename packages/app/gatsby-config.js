module.exports = {
  siteMetadata: {
    title: `Pesticide Risk Reduction Framework`,
    description: `PRRF Description`,
    author: `@gatsbyjs`,
    siteUrl: `https://staging.prrf.sqm.io`,
    brandPartners: [
      {
        label: "OSU",
        path: "https://oregonstate.edu/",
        logo: "/osu-logo.png",
        logoFooter: "/osu.png",
        logoClassName: "width-card",
      },
      {
        label: "USAID",
        path: "https://usaid.gov/",
        logo: "/usaid-logo.svg",
        logoFooter: "/usaid.png",
        logoClassName: "width-card",
      },
      {
        label: "USDA",
        path: "https://www.usda.gov/",
        logo: "/usda-logo.png",
        logoFooter: "/usda.png",
        logoClassName: "width-8",
      },
      {
        label: "Squishymeda",
        path: "https://sqm.io/",
        hideInMainNav: true,
        logoFooter: "/sqm-logo.svg",
        logoClassName: "width-8",
      },
    ],
    mainNavigation: [
      {
        label: "Search",
        path: "/",
      },
    ],
    footerInfo: {
      someText: "this is some text",
    },
    footerNavigation: [
      {
        label: "Home",
        path: "/",
      },
      {
        label: "Page-2",
        path: "/page-2",
      },
    ],

    footerSecondaryNavigation: [
      {
        label: "Terms",
        path: "/",
      },
      {
        label: "Privacy",
        path: "/",
      },
    ],
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: "UA-185940804-1",
      },
    },
    "gatsby-plugin-robots-txt",
    `gatsby-plugin-advanced-sitemap`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-minify`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-emotion`,
      options: {
        // Accepts the following options, all of which are defined by `@emotion/babel-plugin` plugin.
        // The values for each key in this example are the defaults the plugin uses.
        sourceMap: true,
        autoLabel: "dev-only",
        labelFormat: `[local]`,
        cssPropOptimization: true,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#0d1d35`,
        theme_color: `#ffffff`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    //`gatsby-plugin-offline`,
  ],
}
